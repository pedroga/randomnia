SDL=-lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf
FLAGS=-lm -Wall ${SDL}
CDEBUG=gcc ${FLAGS} -ggdb -rdynamic -fstack-protector -O0 -DDEBUG
CRELEASE=gcc ${FLAGS} -O2

# BUILDING FOR WINDOWS:
#  Download:
#  |-> SDL2:           https://www.libsdl.org/release/SDL2-devel-2.0.16-mingw.tar.gz
#  |-> SDL_mixer 2.0:  https://www.libsdl.org/projects/SDL_mixer/release/SDL2_mixer-devel-2.0.4-mingw.tar.gz
#  |-> SDL_ttf 2.0:    https://www.libsdl.org/projects/SDL_ttf/release/SDL2_ttf-devel-2.0.15-mingw.tar.gz
#  |-> SDL_image 2.0:  https://www.libsdl.org/projects/SDL_image/release/SDL2_image-devel-2.0.5-mingw.tar.gz
# Extract and merge all SDL files in the x86_64-w64-mingw32 subfolder from the downloaded packages
# in the same folder (name that folder SDL2) and place it along this makefile.
SDL_WIN_DEPS=-ISDL2 -L SDL2/bin -L SDL2/lib -ISDL2/include/

# Install mingw-w64 package: sudo apt install mingw-w64 (on Debian).
RWINDOWS=x86_64-w64-mingw32-gcc -O2 -DRWINDOWS ${SDL_WIN_DEPS} -lmingw32 -lSDL2main -mwindows ${FLAGS}
OUTNAME=randomnia

#x86_64-w64-mingw32-gcc -O2 -L SDL2/bin -L SDL2/lib -lmingw32 -lSDL2main -mwindows  -lSDL2 -lSDL2_image -lSDL2_ttf -lm -Isrc/ -ISDL2/include/ src/modes/fight_mode.c -c -o ./build/fight_mode.o

#x86_64-w64-mingw32-gcc -I~/code/SDL2_win_headers/SDL2/include/SDL2 -L ~/code/SDL2_win_headers/SDL2/bin -L ~/code/SDL2_win_headers/SDL2/lib -lmingw32 -lSDL2main -mwindows src/main.c -o randomnia.exe

debug: src/main.c
	 ${CDEBUG} src/main.c -o ${OUTNAME}

release: src/main.c
	 ${CREALEASE} src/main.c -o ${OUTNAME}

rwindows: src/main.c
	 mkdir win_build
	 cp SDL2/bin/*.dll win_build
	 ${RWINDOWS} src/main.c -o ${OUTNAME}.exe
	 mv ${OUTNAME}.exe win_build
