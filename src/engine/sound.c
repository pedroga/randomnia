// Musics.
typedef enum {
    MUSIC_NONE,
    MUSIC_THEME, MUSIC_BATTLE,
    MUSIC_TOTAL
} Music;
Mix_Music *g_musics[MUSIC_TOTAL] = {};

// Sound effects.
typedef enum {
    SOUND_VICTORY, SOUND_DEFEAT, SOUND_PUNCH,
    SOUND_TOTAL
} Sound;
Mix_Chunk *g_sounds[SOUND_TOTAL] = {};

int g_volume = 0;

void sound_init(void)
{
    char emsg[ERROR_LEN];
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0) {
        snprintf(emsg, ERROR_LEN, "Mix_OpenAudio() %s\n", Mix_GetError());
        fatal_error(emsg);
    }

    // Load musics.
    g_musics[MUSIC_THEME] =
        Mix_LoadMUS("data/sounds/Its-Always-Sunny-in-the-80s.mp3");
    if (!g_musics[MUSIC_THEME]) {
        snprintf(emsg, ERROR_LEN, "Mix_LoadMUS() %s\n", Mix_GetError());
        fatal_error(emsg);
    }
    g_musics[MUSIC_BATTLE] =
        Mix_LoadMUS("data/sounds/Ominous-in-8-Bit.mp3");
    if (!g_musics[MUSIC_BATTLE]) {
        snprintf(emsg, ERROR_LEN, "Mix_LoadMUS() %s\n", Mix_GetError());
        fatal_error(emsg);
    }
    // Load sounds.
    g_sounds[SOUND_VICTORY] = Mix_LoadWAV("data/sounds/victory.wav");
    if (!g_sounds[SOUND_VICTORY]) {
        snprintf(emsg, ERROR_LEN, "Mix_LoadWAV() %s\n", Mix_GetError());
        fatal_error(emsg);
    }
    g_sounds[SOUND_DEFEAT] = Mix_LoadWAV("data/sounds/defeat.wav");
    if (!g_sounds[SOUND_DEFEAT]) {
        snprintf(emsg, ERROR_LEN, "Mix_LoadWAV() %s\n", Mix_GetError());
        fatal_error(emsg);
    }
    g_sounds[SOUND_PUNCH] = Mix_LoadWAV("data/sounds/hit.wav");
    if (!g_sounds[SOUND_PUNCH]) {
        snprintf(emsg, ERROR_LEN, "Mix_LoadWAV() %s\n", Mix_GetError());
        fatal_error(emsg);
    }
    Mix_VolumeMusic(g_volume * (MIX_MAX_VOLUME / 10));
}

void sound_set_music_volume(int vol) // vol: 0 .. 10
{
    g_volume = vol;
    Mix_VolumeMusic(g_volume * (MIX_MAX_VOLUME / 10));
}

Music g_current_music;
void play_music(Music mus)
{
    if (Mix_PlayMusic(g_musics[mus], -1) < 0) {
        SDL_LogWarn(0, "Mix_PlayMusic() %s\n", Mix_GetError());
    } else {
        g_current_music = mus;
    }
}

void play_sound(Sound sound)
{
    if (Mix_PlayChannel(-1, g_sounds[sound], 0) < 0) {
        SDL_LogWarn(0, "Mix_PlayChannel() %s\n", Mix_GetError());
    }
}

/*
void close()
{
    //Free the sound effects
    Mix_FreeChunk( gScratch );
    Mix_FreeChunk( gHigh );
    Mix_FreeChunk( gMedium );
    Mix_FreeChunk( gLow );
    gScratch = NULL;
    gHigh = NULL;
    gMedium = NULL;
    gLow = NULL;

    //Free the music
    Mix_FreeMusic( gMusic );
    gMusic = NULL;
}
*/
