/* SDL
*****************************************************************************************/
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>

#define ERROR_LEN 128
void fatal_error(char *emsg)
{
    SDL_LogError(0, emsg);
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "SDL ERROR", emsg, NULL);
    exit(1);
}

void warning_error(char *emsg)
{
    SDL_LogWarn(0, emsg);
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "SDL WARNING", emsg, NULL);
}


#define ET(since) (SDL_GetTicks() - (since))

typedef unsigned long long Flag;

#include "geometry.c"
#include "video.c"
#include "input.c"
#include "sound.c"
#include "sprite.c"
#include "animation.c"
#include "vfx.c"

void engine_deinit(void)
{
    puts("Bye.");
    SDL_Quit();
}



#ifndef RWINDOWS /* Only on linux. ************************************************************************************/
#include <execinfo.h>
#include <signal.h>
void segv_handler(int sig)
{
    void *array[10];
    size_t size;
    char **strings;
    size_t i;

    size = backtrace(array, 10);
    strings = backtrace_symbols(array, size);
    printf ("Obtained %zd stack frames.\n", size);
    for (i = 0; i < size; i++)
        printf ("%s\n", strings[i]);
    free (strings);
    abort();
}
#endif /***************************************************************************************************************/

void engine_init(void)
{
    #ifndef RWINDOWS
    {// Set up a stack trace on segfault.
        struct sigaction action = { .sa_handler = segv_handler };
        sigaction(SIGSEGV, &action, NULL);
    }
    #endif
    srand(time(NULL));
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        char emsg[64];
	snprintf(emsg, 64, "[ERROR] SDL_Init: %s\n", SDL_GetError());
        fatal_error(emsg);
    }
    SDL_ShowCursor(SDL_DISABLE);
    video_init();
    sound_init();
    atexit(engine_deinit);
}
