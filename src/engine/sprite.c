/* BASE SPRITE
*****************************************************************************************/
typedef enum {
    ANIM_REPEAT, ANIM_BOUNCE, ANIM_ONCE, ANIM_FREEZE
} Animation_mode;

typedef struct {
    int total_frames;
    int fw, fh; // Frame width and height.
    int refs; // Refcount how many animations are using the texture.
    char *fname;
    Texture *texture;
} Sprite;

void sprite_load(Sprite *sprite)
{   assert(sprite);
    if (sprite->refs++ == 0) {
        assert(!sprite->texture);
        sprite->texture = video_load_texture(sprite->fname);
    }
}

void sprite_free(Sprite *sprite)
{   assert(sprite);
    if (--sprite->refs == 0) {
        video_destroy_texture(&sprite->texture);
        assert(!sprite->texture);
    }
}

/* BATTLE MONSTER SPRITES
*****************************************************************************************/
typedef enum {
    // Small monsters.
    MON_SLIME_COOL, MON_FURMI,
    // Medium monsters.
    MON_SLIME_CUBE,
    // Big monsters.
    MON_SLIME_KING, MON_DRAGON,
    // Huge monsters.
    MON_ENDER_DRAGON,

    MON_TOTAL
} Mon_id;

typedef enum {// Monster animation id.
    MOA_STANDING,
    MOA_ATTACKING,
    MOA_HURTING,
    MOA_DYING,
    MOA_TOTAL
} Moa_id;

typedef struct {
    Sprite sprite;
    struct { int start, end; Animation_mode mode; } animations[MOA_TOTAL];
} Mon_sprite;

Mon_sprite g_mon_atlas[MON_TOTAL] = {
    [MON_SLIME_COOL] = {
        .sprite = {
            .total_frames = 11,
            .fw = 60, .fh = 80,
            .fname = "data/textures/slime_cool.png",
        },
        .animations = {
            [MOA_STANDING]  = { 0,  0, ANIM_REPEAT},
            [MOA_ATTACKING] = { 1,  5, ANIM_ONCE},
            [MOA_HURTING]   = { 6, 10, ANIM_ONCE},
            [MOA_DYING]     = {11, 13, ANIM_FREEZE},
        }
    },
};

/* VFX SPRITES
*****************************************************************************************/
typedef enum {
    // Melee.
    //VFX_CUT,
    VFX_HIT,
    // Elemental.
//    VFX_FIRE,
    VFX_TOTAL
} Vfx_id;

typedef struct {
    Sprite sprite;
    struct { int start, end; } animation;
} Vfx_sprite;

Vfx_sprite g_vfx_atlas[VFX_TOTAL] = {
    [VFX_HIT] = {
        .sprite = {
            .total_frames = 4,
            .fw = 64, .fh = 64,
            .fname = "data/textures/vfx_hit.png",
        },
        .animation = { 0, 3 }
    }
};

/* TOP DOWN SPRITES
*****************************************************************************************/
typedef enum {
    TDSPRITE_VAGRANT,
    TDSPRITE_TOTAL
} Td_spriteid;

typedef enum {// Monster animation id.
    TDANIM_SOUTH,
    TDANIM_EAST,
    TDANIM_WEST,
    TDANIM_NORTH,
    TDANIM_TOTAL
} Td_animationid;

typedef struct {
    Sprite sprite;
    int frames_per_animation; // Every animation has the same amount of frames.
} Td_sprite;

Td_sprite g_td_atlas[TDSPRITE_TOTAL] = {
    [TDSPRITE_VAGRANT] = {
        .sprite = {
            .total_frames = 12,
            .fw = 64, .fh = 64,
            .fname = "data/textures/td_vagrant.png",
        },
        .frames_per_animation = 3,
    },
};

/* ACTION ICONS
*****************************************************************************************/
typedef enum {
    AICON_ATTACK, AICON_DEFEND, AICON_FLEE, AICON_UNKNOWN,
    AICON_TOTAL
} Action_icon;

const char *aicons_fname = "data/textures/action-icons.png";
const Rect aicons_atlas[AICON_TOTAL] = {
    [AICON_ATTACK] = {0, 0, 32, 32},
    [AICON_DEFEND] = {32, 0, 32, 32},
    [AICON_FLEE] = {64, 0, 32, 32},
    [AICON_UNKNOWN] = {96, 0, 32, 32},
};
