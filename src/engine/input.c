#include <SDL2/SDL_gamecontroller.h>

typedef void Key_function(void);

typedef enum {
    INK_UNKNOWN,
    INK_UP, INK_DOWN, INK_LEFT, INK_RIGHT,
    INK_BACK, INK_START,
    INK_A, INK_B, INK_X, INK_Y,
    INK_LB, INK_RB,
    INK_TOTAL
} Input_key;

typedef struct {
    Key_function *keydown_func, *keyup_func;
} Input_keyfunc;

//typedef struct {
//    Input_keyfunc mappings[INK_TOTAL];
//} Input;
typedef Input_keyfunc Input[INK_TOTAL];

// TODO: handle text typing input option and default to scancode for functions.
void input_handle(Input input)
{
    static SDL_GameController *gamepad = NULL;

    void input_connect_controller(void)
    {   assert(!gamepad);
        if (SDL_NumJoysticks() > 0 && SDL_IsGameController(0)) {
            gamepad = SDL_GameControllerOpen(0);
            if (!gamepad) {
                char emsg[ERROR_LEN];
                snprintf(emsg, ERROR_LEN, "SDL_GameControllerOpen(0) %s\n", SDL_GetError());
                warning_error(emsg);
            }
        }
    }

    void input_disconnect_controller(void)
    {   assert(gamepad);
        SDL_GameControllerClose(gamepad);
        gamepad = NULL;
    }

    Input_key input_key_from_sdlkey(SDL_Event event)
    {   assert(event.type == SDL_KEYDOWN || event.type == SDL_KEYUP);
        switch (event.key.keysym.scancode) {
        case SDL_SCANCODE_UP: case SDL_SCANCODE_W: return INK_UP;
        case SDL_SCANCODE_DOWN: case SDL_SCANCODE_S: return INK_DOWN;
        case SDL_SCANCODE_LEFT: case SDL_SCANCODE_A: return INK_LEFT;
        case SDL_SCANCODE_RIGHT: case SDL_SCANCODE_D: return INK_RIGHT;
        case SDL_SCANCODE_RETURN: return INK_A;
        case SDL_SCANCODE_BACKSPACE: return INK_B;
        case SDL_SCANCODE_RCTRL: case SDL_SCANCODE_LCTRL: return INK_X;
        case SDL_SCANCODE_RSHIFT: case SDL_SCANCODE_LSHIFT: return INK_Y;
        case SDL_SCANCODE_ESCAPE: return INK_BACK;
        case SDL_SCANCODE_TAB: return INK_START;
        case SDL_SCANCODE_Q: return INK_LB;
        case SDL_SCANCODE_E: return INK_RB;
        default: return INK_UNKNOWN;
        }
    }

    Input_key input_key_from_sdlgamecontroller(SDL_Event event)
    {   assert(event.type == SDL_CONTROLLERBUTTONDOWN || event.type == SDL_CONTROLLERBUTTONUP);
        // SDL_CONTROLLER_BUTTON_INVALID
        // SDL_CONTROLLER_BUTTON_GUIDE
        // SDL_CONTROLLER_BUTTON_LEFTSTICK
        // SDL_CONTROLLER_BUTTON_RIGHTSTICK
        // SDL_CONTROLLER_BUTTON_MAX
        switch (event.cbutton.button) {
        case SDL_CONTROLLER_BUTTON_DPAD_UP: return INK_UP;
        case SDL_CONTROLLER_BUTTON_DPAD_DOWN: return INK_DOWN;
        case SDL_CONTROLLER_BUTTON_DPAD_LEFT: return INK_LEFT;
        case SDL_CONTROLLER_BUTTON_DPAD_RIGHT: return INK_RIGHT;
        case SDL_CONTROLLER_BUTTON_A: return INK_A;
        case SDL_CONTROLLER_BUTTON_B: return INK_B;
        case SDL_CONTROLLER_BUTTON_X: return INK_X;
        case SDL_CONTROLLER_BUTTON_Y: return INK_Y;
        case SDL_CONTROLLER_BUTTON_BACK: return INK_BACK;
        case SDL_CONTROLLER_BUTTON_START: return INK_START;
        case SDL_CONTROLLER_BUTTON_LEFTSHOULDER: return INK_LB;
        case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER: return INK_RB;
        default: return INK_UNKNOWN;
        }
    }

    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_QUIT) exit(0);
        if (event.type == SDL_CONTROLLERDEVICEADDED && !gamepad) input_connect_controller();
        if (event.type == SDL_CONTROLLERDEVICEREMOVED && gamepad) input_disconnect_controller();
        if (event.type == SDL_KEYDOWN || event.type == SDL_KEYUP) {// Keyboard.
            Input_keyfunc mapping = input[input_key_from_sdlkey(event)];
            if (event.type == SDL_KEYDOWN && mapping.keydown_func)
                mapping.keydown_func();
            else if (event.type == SDL_KEYUP && mapping.keyup_func)
                mapping.keyup_func();
        }
        if (event.type == SDL_CONTROLLERBUTTONDOWN || event.type == SDL_CONTROLLERBUTTONUP) {// Gamepad.
            Input_keyfunc mapping = input[input_key_from_sdlgamecontroller(event)];
            if (event.type == SDL_CONTROLLERBUTTONDOWN && mapping.keydown_func)
                mapping.keydown_func();
            else if (event.type == SDL_CONTROLLERBUTTONUP && mapping.keyup_func)
                mapping.keyup_func();
        }
    }
}
