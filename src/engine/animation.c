/* DECLARATIONS
*****************************************************************************************/
#define ANIMATION_DELAY 120
#define VFX_DELAY 120

typedef enum {
    SK_MONSTER, SK_VFX, SK_TOPDOWN,
} Sprite_kind; // From which atlas the sprite comes from.

typedef struct {
    Sprite_kind kind;
    int id; // References the sprite on the respective atlas.
    int cframe, cline;
    unsigned last_update;
    bool is_playing;
} Animation;

/* DEFINITIONS
*****************************************************************************************/
Animation animation_create(Sprite_kind kind, unsigned id)
{
    switch (kind) {
    case SK_MONSTER: {
        assert(id < MON_TOTAL);
        sprite_load(&g_mon_atlas[id].sprite);
    } break;
    case SK_VFX: {
        assert(id < VFX_TOTAL);
        sprite_load(&g_vfx_atlas[id].sprite);
    } break;
    case SK_TOPDOWN: {
        assert(id < TDSPRITE_TOTAL);
        sprite_load(&g_td_atlas[id].sprite);
    } break;
    default: assert(false);
    }
    Animation anim = (Animation) { .kind = kind, .id = id, .is_playing = true };
    return anim;
}

Rect animation_get_src(Animation *anim)
{   assert(anim);
    Rect src;
    switch (anim->kind) {
    case SK_MONSTER: {
        Sprite sprite = g_mon_atlas[anim->id].sprite;
        src = (Rect) {
            .x = sprite.fw * anim->cframe, .y = 0,
            .w = sprite.fw, .h = sprite.fh
        };
    } break;
    case SK_VFX: {
        Sprite sprite = g_vfx_atlas[anim->id].sprite;
        src = (Rect) {
            .x = sprite.fw * anim->cframe, .y = 0,
            .w = sprite.fw, .h = sprite.fh
        };
    } break;
    case SK_TOPDOWN: {
        Sprite sprite = g_td_atlas[anim->id].sprite;
        src = (Rect) {
            .x = sprite.fw * anim->cframe, .y = 0,
            .w = sprite.fw, .h = sprite.fh
        };
    } break;
    default: assert(false);
    };
    return src;
}

// TODO: ANIM_BOUNCE
bool animation_draw(Animation *anim, Rect dst)
{   assert(anim);
    switch (anim->kind) {
    case SK_MONSTER: {
        if (anim->is_playing && ET(anim->last_update) > ANIMATION_DELAY) {
            anim->last_update = SDL_GetTicks();
            if (++anim->cframe > g_mon_atlas[anim->id].animations[anim->cline].end) {
                switch (g_mon_atlas[anim->id].animations[anim->cline].mode) {
                case ANIM_REPEAT: {
                    anim->cframe = g_mon_atlas[anim->id].animations[anim->cline].start;
                } break;
                case ANIM_ONCE: {
                    anim->cframe = 0;
                    anim->is_playing = false;
                } break;
                default: {// Freeze animation on last frame.
                    --anim->cframe;
                    anim->is_playing = false;
                }
                }
            }
        }
        Rect src = animation_get_src(anim);
        video_draw_texture(g_mon_atlas[anim->id].sprite.texture, &src, &dst);
        return anim->is_playing;
    } break;
    case SK_VFX: {
        assert(anim->is_playing); // Vfx are never frozen.
        if (ET(anim->last_update) > VFX_DELAY) {
            anim->last_update = SDL_GetTicks();
            if (++anim->cframe > g_vfx_atlas[anim->id].animation.end) {
                anim->cframe = 0;
                anim->is_playing = false;
            }
        }
        Rect src = animation_get_src(anim);
        video_draw_texture(g_vfx_atlas[anim->id].sprite.texture, &src, &dst);
        return anim->is_playing;
    } break;
    case SK_TOPDOWN: {
        if (anim->is_playing && ET(anim->last_update) > ANIMATION_DELAY) {
            anim->last_update = SDL_GetTicks();
            if (++anim->cframe >= g_td_atlas[anim->id].frames_per_animation * (anim->cline + 1)) {
                anim->cframe = anim->cline * g_td_atlas[anim->id].frames_per_animation;
//                anim->is_playing = false;
            }
        }
        Rect src = animation_get_src(anim);
        video_draw_texture(g_td_atlas[anim->id].sprite.texture, &src, &dst);
        return anim->is_playing;
    } break;
    default: assert(false);
    }
}

void animation_set_line(Animation *anim, unsigned line)
{   assert(anim);
    switch (anim->kind) {
    case SK_MONSTER: {
        assert(line < MOA_TOTAL);
        anim->cline = line; anim->last_update = 0; anim->is_playing = true;
        anim->cframe = g_mon_atlas[anim->id].animations[line].start;
    } break;
    case SK_VFX: assert(false); // Vfx have only one line.
    case SK_TOPDOWN: {
        assert(line < TDANIM_TOTAL);
        if (line != anim->cline) {
            anim->cframe = g_td_atlas[anim->id].frames_per_animation * line;
            anim->cline = line; anim->last_update = 0; anim->is_playing = true;
        }
    } break;
    default: assert(false);
    }
}

void animation_resume(Animation *anim)
{   assert(anim);
    anim->is_playing = true;
}

void animation_pause(Animation *anim)
{   assert(anim);
    anim->is_playing = false;
}

void animation_destroy(Animation *anim)
{
    switch (anim->kind) {
    case SK_MONSTER: {
        sprite_free(&g_mon_atlas[anim->id].sprite);
    } break;
    case SK_VFX: {
        sprite_free(&g_vfx_atlas[anim->id].sprite);
    } break;
    case SK_TOPDOWN: {
        sprite_free(&g_td_atlas[anim->id].sprite);
    } break;
    default: assert(false);
    }
}
