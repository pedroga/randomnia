typedef SDL_Rect Rect;
typedef SDL_Point Point;

const Rect null_rect = (Rect) {0, 0, 0, 0};

bool rect_equals(Rect a, Rect b)
{
    return !memcmp(&a, &b, sizeof(Rect));
}

bool is_rect_empty(Rect a)
{
    Rect zero_rect = (Rect) {0, 0, 0, 0};
    return rect_equals(a, zero_rect);
}
