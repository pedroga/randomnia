/* COLORS
*******************************************************************************/
typedef SDL_Color Color;
// Base colors.
#define COLOR_BLACK (Color) {0, 0, 0, 255}
#define COLOR_TRANS_BLACK (Color) {0, 0, 0, 0x20}
#define COLOR_WHITE (Color) {255, 255, 255, 255}
#define COLOR_RED (Color) {255, 0, 0, 255}
#define COLOR_AMBER (Color) {0xFF, 0xBF, 0x00, 0xFF}
#define COLOR_LAWN_GREEN (Color) {0x84, 0xDE, 0x02, 0xFF}
#define COLOR_SEA_GREEN (Color) {0x3B, 0x7A, 0x57, 0xFF}
#define COLOR_ALMOND (Color) {0xEF, 0xDE, 0xCD, 0xFF}
#define COLOR_AMARANTH (Color) {0x00, 0xCC, 0x22, 0xFF}
// Used colors.
#define COLOR_TITLE (Color) {172, 23, 96, 255}
#define COLOR_PRIMARY_BTN (Color) {96, 48, 64, 255}
#define COLOR_SECONDARY_BTN (Color) {64, 32, 48, 128}
#define COLOR_CURSOR (Color) {196, 64, 64, 255}
#define COLOR_BATTLE_MENU COLOR_ALMOND
#define COLOR_BATTLEFIELD COLOR_SEA_GREEN
#define COLOR_ICON_NUMBER COLOR_AMARANTH
#define COLOR_TURN_TIMER COLOR_AMBER
#define COLOR_WOUNDL_UNHURT           ((Color) {0x3B, 0x7A, 0x57, 0xFF})
#define COLOR_WOUNDL_SCRATCHED        ((Color) {0xB0, 0xBF, 0x1A, 0xFF})
#define COLOR_WOUNDL_HURT             ((Color) {0xFF, 0xBF, 0x00, 0xFF})
#define COLOR_WOUNDL_VERY_HURT        ((Color) {0xAF, 0x00, 0x2A, 0xFF})
#define COLOR_WOUNDL_INCAPACITATED    ((Color) {0x4D, 0x00, 0x00, 0xFF})
#define COLOR_WOUNDL_DEAD             ((Color) {0x33, 0x00, 0x09, 0xFF})
#define COLOR_LOG_PLAYER_1  ((Color) {0x00, 0x4D, 0x1A, 255})
#define COLOR_LOG_PLAYER_2  ((Color) {0x00, 0x99, 0x66, 255})
#define COLOR_LOG_MONSTER_1 ((Color) {0x66, 0x00, 0x44, 255})
#define COLOR_LOG_MONSTER_2 ((Color) {0x95, 0x00, 0xB3, 255})
#define COLOR_STATUS_MODE_MENU ((Color) {0x42, 0x42, 0x42, 0xF0})
#define COLOR_MENU      ((Color) {0x42, 0x42, 0x42, 0xF0})
#define COLOR_MENU_DESC ((Color) {0x21, 0x21, 0x21, 0xAA})
const Color dlcolors[] = {
    /*DL_TERRIBLE*/ {0x4D, 0x00, 0x00},
    /*DL_POOR*/     {0x99, 0x19, 0x00},
    /*DL_MEDIOCRE*/ {0xCC, 0x44, 0x00},
    /*DL_FAIR*/     {0xE6, 0xBF, 0x00},
    /*DL_GOOD*/     {0x66, 0xCC, 0x00},
    /*DL_GREAT*/    {0x00, 0xB3, 0x3C},
    /*DL_SUPERB*/   {0x00, 0x80, 0x55},
};

/* DECLARATIONS
*******************************************************************************/
typedef SDL_Texture Texture;
typedef struct {int w, h;} Dim;
typedef struct {Texture *texture; int len;} Text;

void video_init(void);
void video_deinit(void);
void video_clear(void);
void video_present(void);
void video_draw_rect(Rect dst, Color);
void video_fill_rect(Rect dst, Color);
void video_fill_box(Rect dst, int border, Color box_color, Color border_color);
void video_draw_line(Point from, Point to, Color);
void video_set_texture_color_mod(Texture *, Color);
Texture *video_render_gradient(void);
void video_draw_texture(Texture *, const Rect *src, const Rect *dst);
void video_draw_texture_to(Texture *, const Rect *src, const Rect *dst, Texture *);
Texture *video_load_texture(const char *fname);
Texture *video_load_texture_from_text(const char *text, SDL_Color color);
Text video_load_text(const char *text, SDL_Color color);
void video_destroy_texture(Texture **texture);

/* GLOBALS
*******************************************************************************/
#define VIDEO_W 1600
#define VIDEO_H 900
// Instead of using absolute pixel positioning, use these.
#define HU (VIDEO_W/20) // Horizontal unit -> 80 pixels.
#define VU (VIDEO_H/20) // Vertical unit -> 45 pixels.

#define DEFAULT_FONT_WIDTH 20

static SDL_DisplayMode display_mode;
static SDL_Window *g_window = NULL;
static SDL_Renderer *g_renderer = NULL;
static TTF_Font *g_font = NULL;
static SDL_Texture *g_texture = NULL;

/* IMPLEMENTATION
*******************************************************************************/
void video_init(void)
{
    char emsg[ERROR_LEN];
    if (!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG)) {
	snprintf(emsg, ERROR_LEN, "IMG_Init() %s\n", IMG_GetError());
        fatal_error(emsg);
    }
    if (TTF_Init() == -1) {
        snprintf(emsg, ERROR_LEN, "TTF_Init() %s\n", TTF_GetError());
        fatal_error(emsg);
    }
    if (SDL_GetDesktopDisplayMode(0, &display_mode) < 0) {
        snprintf(emsg, ERROR_LEN, "SDL_GetDesktopDisplayMode() %s\n", SDL_GetError());
        fatal_error(emsg);
    }
    Rect display_bounds = {};
    if (SDL_GetDisplayBounds(0, &display_bounds) < 0) {
        snprintf(emsg, ERROR_LEN, "SDL_GetDisplayBounds() %s\n", SDL_GetError());
        fatal_error(emsg);
    }
    if (!(g_window = SDL_CreateWindow(
              "Randomnia", display_bounds.x, display_bounds.y,
              display_mode.w, display_mode.h, SDL_WINDOW_FULLSCREEN)))
    {
        snprintf(emsg, ERROR_LEN, "SDL_CreateWindow() %s\n", SDL_GetError());
        fatal_error(emsg);
    }
    if (!(g_renderer = SDL_CreateRenderer(g_window, -1,
                                          SDL_RENDERER_ACCELERATED |
                                          SDL_RENDERER_PRESENTVSYNC |
                                          SDL_RENDERER_TARGETTEXTURE))) {
        snprintf(emsg, ERROR_LEN, "SDL_CreateRenderer() %s\n", SDL_GetError());
        fatal_error(emsg);
    }
    if (SDL_SetRenderDrawBlendMode(g_renderer, SDL_BLENDMODE_BLEND)) {
        snprintf(emsg, ERROR_LEN, "SDL_SetRenderDrawBlendMode() %s\n", SDL_GetError());
        fatal_error(emsg);
    }
    if (!(g_texture = SDL_CreateTexture(g_renderer, display_mode.format,
                                        SDL_TEXTUREACCESS_TARGET,
                                        VIDEO_W, VIDEO_H))) {
        snprintf(emsg, ERROR_LEN, "SDL_CreateTexture() %s\n", SDL_GetError());
        fatal_error(emsg);
    }
    if (!(g_font = TTF_OpenFont("data/fonts/default-font.ttf", 16))) {
        snprintf(emsg, ERROR_LEN, "TTF_OpenFont() %s\n", TTF_GetError());
        fatal_error(emsg);
    }

}

void video_clear(void)
{
    SDL_SetRenderTarget(g_renderer, g_texture);
    SDL_SetRenderDrawColor(g_renderer, 0, 0, 0, 255);
    SDL_RenderClear(g_renderer);
}

void video_present(void)
{
    SDL_SetRenderTarget(g_renderer, NULL);
    SDL_RenderCopy(g_renderer, g_texture, NULL, NULL);
    SDL_RenderPresent(g_renderer);
}

void video_draw_rect(Rect dst, Color c)
{   assert(dst.w > 0 && dst.h > 0);
    SDL_SetRenderDrawColor(g_renderer, c.r, c.g, c.b, c.a);
    SDL_RenderDrawRect(g_renderer, &dst);
}

void video_fill_rect(Rect dst, Color c)
{   assert(dst.w >= 0 && dst.h >= 0);
    SDL_SetRenderDrawColor(g_renderer, c.r, c.g, c.b, c.a);
    SDL_RenderFillRect(g_renderer, &dst);
}

void video_fill_box(Rect dst, int border, Color box_color, Color border_color)
{   assert(dst.w >= 0 && dst.h >= 0);
    Rect inner_dst = {
        .x = dst.x + border, .w = dst.w - border * 2,
        .y = dst.y + border, .h = dst.h - border * 2,
    };
    Rect left_border   = {.x = dst.x, .w = border, .y = dst.y, .h = dst.h};
    Rect right_border  = {.x = dst.x + dst.w - border, .w = border, .y = dst.y, .h = dst.h};
    Rect top_border    = {.x = dst.x, .w = dst.w, .y = dst.y, .h = border};
    Rect bottom_border = {.x = dst.x, .w = dst.w, .y = dst.y + dst.h - border, .h = border};
    video_fill_rect(left_border, border_color);
    video_fill_rect(right_border, border_color);
    video_fill_rect(top_border, border_color);
    video_fill_rect(bottom_border, border_color);
    video_fill_rect(inner_dst, box_color);
}

void video_set_texture_color_mod(Texture *texture, Color c)
{
    SDL_SetTextureColorMod(texture, c.r, c.g, c.b);
}

Texture *video_render_gradient(void)
{
    char emsg[ERROR_LEN];
    Texture *gradient = SDL_CreateTexture(
        g_renderer, display_mode.format, SDL_TEXTUREACCESS_TARGET, VIDEO_W, VIDEO_H);
    if (!gradient) {
        snprintf(emsg, ERROR_LEN, "SDL_CreateTexture() %s\n", SDL_GetError());
        fatal_error(emsg);
    }

    SDL_SetRenderTarget(g_renderer, gradient);
    for (int i = 0; i < VIDEO_H; ++i) {
        Point x1 = {0, i};
        Point x2 = {VIDEO_W, i};
        Color c = {255, 255, 255, 255};
        c.g = (int)((double)i / ((double)VIDEO_H / 256)) % 256;
        video_draw_line(x1, x2, c);
    }
    SDL_SetRenderTarget(g_renderer, g_texture);

    return gradient;
}

void video_draw_line(Point from, Point to, Color c)
{
    SDL_SetRenderDrawColor(g_renderer, c.r, c.g, c.b, c.a);
    SDL_RenderDrawLine(g_renderer, from.x, from.y, to.x, to.y);
}

void video_draw_texture(Texture *texture, const Rect *src, const Rect *dst)
{   assert(texture);
    SDL_RendererFlip flip = SDL_FLIP_NONE;
/*
    if (dst && dst->w < 0) { // Negative dst->w means that it should be flipped.
        flip = SDL_FLIP_HORIZONTAL;
        dst->w = -dst->w;
    }
*/
    if (SDL_RenderCopyEx(g_renderer, texture, src, dst, 0, NULL, flip) < 0) {
        char emsg[ERROR_LEN];
        snprintf(emsg, ERROR_LEN, "SDL_RenderCopy() %s\n", SDL_GetError());
        fatal_error(emsg);
    }
}

void video_draw_texture_to(Texture *from, const Rect *src, const Rect *dst, Texture *to)
{   assert(from && to);
    SDL_SetRenderTarget(g_renderer, to);
    video_draw_texture(from, src, dst);
    SDL_SetRenderTarget(g_renderer, g_texture);
}

Texture *video_texture_from_texture(Texture *from, Rect src)
{
    Texture *texture = SDL_CreateTexture(g_renderer, display_mode.format, SDL_TEXTUREACCESS_TARGET, src.w, src.h);
    video_draw_texture_to(from, &src, NULL, texture);
    return texture;
}

Texture *video_load_texture(const char *fname)
{   assert(fname);
    char emsg[ERROR_LEN];

    SDL_Surface *surface = IMG_Load(fname);
    if (!surface) {
	snprintf(emsg, ERROR_LEN, "IMG_Load() %s\n", IMG_GetError());
        fatal_error(emsg);
    }
    SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, 0xFF, 0, 0xFF));
    Texture *texture = SDL_CreateTextureFromSurface(g_renderer, surface);
    if (!texture) {
        snprintf(
            emsg, ERROR_LEN, "SDL_CreateTextureFromSurface() %s\n",
            SDL_GetError());
        fatal_error(emsg);
    }
    SDL_FreeSurface(surface);
    return texture;

}

Texture *video_load_texture_from_text(const char *text, Color c)
{   assert(text);
    char emsg[ERROR_LEN];

    SDL_Surface *surface = TTF_RenderText_Solid(g_font, text, c);
    if (!surface) {
        snprintf(emsg, ERROR_LEN, "TTF_RenderText_Solid() %s\n", TTF_GetError());
        fatal_error(emsg);
    }
    Texture *texture = SDL_CreateTextureFromSurface(g_renderer, surface);
    if (!texture) {
        snprintf(
            emsg, ERROR_LEN,
            "SDL_CreateTextureFromSurface() %s\n", SDL_GetError());
        fatal_error(emsg);
    }
    SDL_FreeSurface(surface);
    return texture;
}

Text video_load_text(const char *text, Color c)
{   assert(text);
    return (Text) {video_load_texture_from_text(text, c), strlen(text)};
}

void video_destroy_texture(Texture **texture)
{   assert(texture);
    if (*texture)
        SDL_DestroyTexture(*texture);
    // TODO: check for error.
    *texture = NULL;
}

/* From: https://stackoverflow.com/questions/6804866/changing-pixel-color-in-sdl
void setColor(SDL_Surface *surface, SDL_Color color) {
        Uint16 *pixels = (Uint16 *) surface->pixels;            // Get the pixels from the Surface

        // Iterate through the pixels and change the color
        for (int i = 0; i w * surface->h); i++) {
            if (pixels[i] == SDL_MapRGB(surface->format, 0xFF, 0xFF, 0xFF))        // Search for white pixels
                pixels[i] = SDL_MapRGB(surface->format, color.r, color.b, color.g);
        }
    }
*/
