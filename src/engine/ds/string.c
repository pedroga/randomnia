#define STRING_ALLOC_SZ 16

typedef struct {
    char *data;
    int len, cap;
} String;

String string_create(char *data)
{
    String str = {};
    if (data) {
        if ((str.data = strdup(data))) str.len = str.cap = strlen(str.data);
        else fatal_error("string_create(): Couldn't duplicate string!");
    } else {
        if (!(str.data = malloc(sizeof(char) * (str.cap = STRING_ALLOC_SZ))))
            fatal_error("string_create(): Couldn't malloc string!");
    }
    return str;
}

void string_addch(String *str, char c)
{
    
}
