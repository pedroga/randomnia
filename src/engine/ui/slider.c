typedef struct {
    int min, max, cur, step;
    bool continuous;
} Slider;

void slider_draw(Slider slider, Rect dst)
{
    const int border = 4;
    Rect central_line = {dst.x, dst.y + dst.h/2 - border/2, dst.x + dst.w, dst.y + dst.h/2 + border/2};
    video_fill_rect(central_line, COLOR_WHITE);
}
