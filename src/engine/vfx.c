#define VFX_MAX_EFFECTS 10

typedef struct {
    Animation animation;
    Rect dst;
} Vfx;

Vfx _vfx[VFX_MAX_EFFECTS] = {};
int _vfx_count = 0;

void vfx_play(Vfx_id id, Rect dst)
{   assert(id < VFX_TOTAL && _vfx_count < VFX_MAX_EFFECTS);
    _vfx[_vfx_count++] = (Vfx) {
        .animation = animation_create(SK_VFX, id),
        .dst = dst,
    };
}

void vfx_draw(void)
{
    for (int i = 0; i < _vfx_count; ++i) {
        bool ended = !animation_draw(&_vfx[i].animation, _vfx[i].dst);
        if (ended) {
            animation_destroy(&_vfx[i].animation);
            _vfx[i] = _vfx[--_vfx_count];
        }
    }
}

void vfx_clear(void)
{
    _vfx_count = 0;
}
