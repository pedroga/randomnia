#include <time.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>

#include "utils.c"
#include "engine/engine.c"
#include "common/common.c"
#include "modes/modes.c"

int main(int argc, char **argv)
{
    engine_init();
    title_mode();
    return 0;
}
