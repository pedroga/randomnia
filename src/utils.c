/* LOGGERS
***********************************************************************************************************************/
#ifdef DEBUG
struct _logger {
    const char * const func_name;
};
inline __attribute__((always_inline))
void _enter(struct _logger *logger)
{
    fprintf(stdout, "enter: %s\n", logger->func_name);
}
inline __attribute__((always_inline))
void _leave(struct _logger *logger)
{
    fprintf(stdout, "leave: %s\n", logger->func_name);
}
#define log_calltrace()                                 \
    __attribute__((cleanup(_leave))) struct _logger     \
    _log = {.func_name = __func__};                     \
    _enter(&_log);
#else
#define log_calltrace() {}
#endif

#ifdef DEBUG
#define log_fps_start()                         \
    unsigned _fps_start = SDL_GetTicks();       \
    int _frames_rendered = 0;
#define log_fps()                                                       \
    unsigned _elapsed_time = (SDL_GetTicks() - _fps_start) / 1000;      \
    if (_elapsed_time > 0)                                              \
        printf("[FPS] %d (%d frames rendered)\n",                       \
               _frames_rendered / _elapsed_time,                        \
               _frames_rendered);                                       \
    ++_frames_rendered;
#else
#define log_fps_start() {}
#define log_fps() {}
#endif

/* CONTRACTS
***********************************************************************************************************************/
#define in(precondition)                                                \
    if (!(precondition)) {                                              \
        fprintf(stderr, "[%s:%d:%s()] Precondition failed: %s\n",       \
                __FILE__, __LINE__, __func__, #precondition);           \
        exit(1);                                                        \
    }

#define out(postcondition)                                              \
    void out_block_(const char **fname) {                               \
        if (!(postcondition)) {                                         \
            fprintf(stderr, "[%s:%d:%s()] Postcondition failed: %s\n",  \
                    __FILE__, __LINE__, *fname, #postcondition);        \
            exit(1);                                                    \
        }                                                               \
    }                                                                   \
    __attribute__((cleanup(out_block_))) const char *out_ = __func__;

/* DEFER
***********************************************************************************************************************/
#define defer(x, fn)                            \
    for (int i##__LINE__ = 0;                   \
         i##__LINE__ == 0;                      \
         ++i##__LINE__, fn((x)))

/* LAMBDA
***********************************************************************************************************************/
// When there are commas ',' in the function_body, call the lambda using statement exprs like this:
//   lambda(rettype, (params) {({ body })})
#define lambda(return_type, function_body)                      \
    ({                                                          \
        return_type my_anonymous_function function_body;        \
        my_anonymous_function;                                  \
    })

/* SOME FUNCTIONS
***********************************************************************************************************************/
#define sign(x) (((x) > 0) - ((x) < 0))

#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))

// TODO: use typeof instead of int.
#define clamp(_v, _lo, _hi) ({                          \
    /* Only evaluate each param once. */                \
    int lo = (_lo), hi = (_hi), v = (_v);               \
    int res = v;                                        \
    if (v < lo) res = lo; else if (v > hi) res = hi;    \
    res; })

// TODO: lerp -> https://www.febucci.com/2018/08/easing-functions/
// #define lerp(

/* SOME DEFINITIONS
***********************************************************************************************************************/
#define KB 1024
#define MB (KB * KB)
#define GB (MB * KB)
