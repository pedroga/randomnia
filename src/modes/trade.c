void trade_mode(Func_redraw *parent_redraw, int shop_id)
{   assert(parent_redraw);

    Rect dst = {.x = HU * 11, .w = HU * 8, .y = VU * 1, .h = VU * 18};

    {// Mode loop.
        bool loop = true;
        Input input = {
            [INK_BACK] = { lambda(void, () { loop = false; }) },
        };

        void redraw(void)
        {
            parent_redraw();
            video_fill_box(dst, 3, COLOR_TRANS_BLACK, COLOR_WHITE);
//            video_draw_texture(bg_texture, NULL, &bg_dst);
        }

        void update(void)
        {

        }

        while (loop) {
            if (g_current_music != MUSIC_BATTLE)
                play_music(MUSIC_BATTLE); // TODO: town_music.
            video_clear();
            input_handle(input);
            update();
            redraw();
            video_present();
        }
    }
    {// Deinit.
    }
}
