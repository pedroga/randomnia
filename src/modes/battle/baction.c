#define BACTION_MAX 5

typedef enum {
    BAC_NONE, BAC_ATTACK, BAC_DEFEND, BAC_FLEE
} Baction_id;

typedef struct {
    Baction_id id;
    int target;
    uint32_t start_time, required_time;
} Baction;

typedef struct {
    Baction actions[BACTION_MAX];
    int len;
} Baction_q;

Texture *g_aicons = NULL;

void baction_init(void)
{
    if (!g_aicons)
        g_aicons = video_load_texture(aicons_fname);
}

void baction_draw(Baction action, Rect dst, bool draw_timer)
{
    // TODO: move this to video.c.
    static Texture *numbers[10] = {};
    if (!numbers[0]) {
        char aux[2] = {};
        for (int i = 0; i < 10; ++i) {
            aux[0] = '0' + i;
            numbers[i] = video_load_texture_from_text(aux, COLOR_ICON_NUMBER);
        }
    }
    int aicon_idx = AICON_UNKNOWN;
    switch (action.id) {
    case BAC_NONE: {
    } break;
    case BAC_ATTACK: {
        aicon_idx = AICON_ATTACK;
    } break;
    case BAC_DEFEND: {
        aicon_idx = AICON_DEFEND;
    } break;
    case BAC_FLEE: {
        aicon_idx = AICON_FLEE;
    } break;
    }
    video_draw_texture(g_aicons, &aicons_atlas[aicon_idx], &dst);
    {// Draw target slot.
        Rect target_dst = dst;
        target_dst.x += target_dst.w / 2; target_dst.y += target_dst.h / 2;
        target_dst.w /= 2; target_dst.h /= 2;
        video_draw_texture(numbers[action.target], NULL, &target_dst);
    }
    {// Draw current action timer bar.
        if (draw_timer) {
            Rect timer_dst = dst;
            unsigned turn_et = SDL_GetTicks() - action.start_time;
            timer_dst.h = dst.h / 10;
            timer_dst.w = ((double)dst.w / action.required_time) *
                ((double)action.required_time - turn_et);
            if (timer_dst.w < 0) timer_dst.w = 0;
            video_fill_rect(timer_dst, COLOR_TURN_TIMER);
        }
    }
}

void baction_qadd(Baction_q *q, Baction action)
{
    if (q->len < BACTION_MAX) {
        action.start_time = SDL_GetTicks();
        q->actions[q->len++] = action;
    }
}

Baction baction_qget(Baction_q *q)
{
    if (q->len > 0) {
        Baction act = q->actions[0];
        for (int i = 1; i < q->len; ++i) {
            q->actions[i - 1] = q->actions[i];
        } q->len--;
        if (q->len > 0)
            q->actions[0].start_time = SDL_GetTicks();
        return act;
    } else {
        return (Baction) {.id = BAC_NONE};
    }
}

void baction_qcancel(Baction_q *q)
{
    q->len--;
}
