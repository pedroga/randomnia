#define BMENU_CHAR_WIDTH 20

enum {
    BMENU_START = -1,
    BMENU_ATTACK = 0, BMENU_DEFEND, BMENU_FLEE,
    BMENU_END
};

typedef struct {
    Rect box;
    Baction_q action_q;
    Text cmds[BMENU_END];
    int selcmd;
} Bmenu;

Bmenu bmenu_create(Rect box)
{
    Bmenu menu = (Bmenu) {
        .box = box,
        .cmds = {
            [BMENU_ATTACK] = video_load_text("Attack", COLOR_WHITE),
            [BMENU_DEFEND] = video_load_text("Defend", COLOR_WHITE),
            [BMENU_FLEE] = video_load_text("Flee", COLOR_WHITE),
        },
        .selcmd = 0
    };
    return menu;
}

void bmenu_destroy(Bmenu *menu)
{   assert(menu);
    for (int i = 0; i < 3; ++i) video_destroy_texture(&menu->cmds[i].texture);
}

void bmenu_next(Bmenu *menu)
{   assert(menu);
    if (++menu->selcmd >= BMENU_END) menu->selcmd = BMENU_START + 1;
}

void bmenu_prev(Bmenu *menu)
{   assert(menu);
    if (--menu->selcmd <= BMENU_START) menu->selcmd = BMENU_END - 1;
}

void bmenu_draw(Bmenu *menu)
{   assert(menu);
    video_fill_box(menu->box, 5, COLOR_BATTLE_MENU, COLOR_BLACK);
    {// Draw action queue.
        Rect div = menu->box; div.y += 37; div.h = 5;
        video_fill_rect(div, COLOR_BLACK);
        for (int i = 0; i < menu->action_q.len; ++i) {
            Rect dst = {
                .x = menu->box.x + i * 32 + 5, .w = 32,
                .y = menu->box.y + 5, .h = 32,
            };
            baction_draw(menu->action_q.actions[i], dst, i == 0);
        }
    }
    {// Draw cmds.
        Rect cmd_dst = {
            .x = menu->box.x + 6, .w = menu->box.w - 12,
            .y = menu->box.y + 42, .h = 25
        };
        for (int i = BMENU_START + 1; i < BMENU_END; ++i) {
            video_set_texture_color_mod(
                menu->cmds[i].texture,
                menu->selcmd == i ? COLOR_PRIMARY_BTN : COLOR_BLACK
            );
            cmd_dst.w = min(menu->box.w, menu->cmds[i].len * BMENU_CHAR_WIDTH);
            video_draw_texture(menu->cmds[i].texture, NULL, &cmd_dst);
            cmd_dst.y += cmd_dst.h + 5;
        }
    }
}
