/* BMON DEFINITIONS
*****************************************************************************************/
typedef struct {
    Stats stats;
    Animation animation;
    Texture *target;
    Rect slot, dst;
    Baction_q aq;
} Bmon; // Battle_monster.

// How many slots they occupy.
const int bmon_size[MON_TOTAL] = {
    [MON_SLIME_COOL] = 1,
    [MON_SLIME_CUBE] = 2,
    [MON_FURMI] = 1,
    [MON_SLIME_KING] = 3,
    [MON_DRAGON] = 3,
    [MON_ENDER_DRAGON] = 6,
};

/* BMON IMPLEMENTATION
*****************************************************************************************/
#define BMON_SCALE 3

Bmon bmon_create(Mon_id id, Rect slot)
{
    Bmon mon = (Bmon) {
        .stats = rpgs_create_monster(id),
        .animation = animation_create(SK_MONSTER, id),
        .target = video_load_texture("data/textures/arrow_target.png"),
        .slot = slot,
    };
    Rect src = animation_get_src(&mon.animation);
    mon.dst.w = src.w * BMON_SCALE; mon.dst.h = src.h * BMON_SCALE;
    mon.dst.x = slot.x + slot.w / 2 - mon.dst.w / 2;
    mon.dst.y = slot.y + slot.h - mon.dst.h;
    return mon;
}

void bmon_draw(Bmon *mon, bool target)
{
    {// TEMP: draw the slot in the back.
        //video_fill_rect(mon->slot, COLOR_SECONDARY_BTN);
        video_draw_rect(mon->slot, COLOR_PRIMARY_BTN);
    }
    animation_draw(&mon->animation, mon->dst);
    if (target) {// Draw arrow on top of monster if it's being targeted.
        //video_draw_rect(mon->dst, COLOR_CURSOR);
        Rect target_dst = (Rect) {
            .x = mon->dst.x + mon->dst.w / 2, .w = 64,
            .y = mon->dst.y - 72, .h = 64
        };
        {// Compute arrow color by monster health.
            Color c = {255, 255, 255};
            switch (wound_level(mon->stats)) {
            case WOUNDL_UNDAMAGED:
                c = COLOR_WOUNDL_UNHURT; break;
            case WOUNDL_SCRATCHED:
                c = COLOR_WOUNDL_SCRATCHED; break;
            case WOUNDL_HURT:
                c = COLOR_WOUNDL_HURT; break;
            case WOUNDL_VERY_HURT:
                c = COLOR_WOUNDL_VERY_HURT; break;
            case WOUNDL_INCAPACITATED:
                c = COLOR_WOUNDL_INCAPACITATED; break;
            case WOUNDL_DEAD:
                c = COLOR_WOUNDL_DEAD; break;
            default: assert(false);
            };
            video_set_texture_color_mod(mon->target, c);
        }
        target_dst.x -= target_dst.w / 2;
        video_draw_texture(mon->target, NULL, &target_dst);
    }
    Rect aicon_dst = {
        .x = mon->dst.x, .y = mon->dst.y + mon->dst.h,
        .w = 32, .h = 32,
    };
    video_draw_texture(g_aicons, &aicons_atlas[AICON_UNKNOWN], &aicon_dst);
    /* TODO: show revealed actions.
    for (int i = 0; i < mon->actions.len; ++i) {
        Rect aicon_dst = {
            .x = mon->dst.x + 32 * i, .y = mon->dst.y + mon->dst.h,
            .w = 32, .h = 32,
        };
        //video_draw_texture(g_aicons, &aicons_atlas[mon->], &aicon_dst);
    }
    */
}

void bmon_destroy(Bmon *mon)
{
    animation_destroy(&(mon->animation));
    video_destroy_texture(&mon->target);
}

void bmon_attack(Bmon *mon, int target)
{
    animation_set_line(&mon->animation, MOA_ATTACKING);
}

void bmon_attacked(Bmon *mon, Attack atk)
{
    vfx_play(VFX_HIT, mon->dst);
    if (wound_level(mon->stats) != WOUNDL_DEAD) {// Leave it alone already!
        if (wound_level(mon->stats) == WOUNDL_DEAD) {
            animation_set_line(&mon->animation, MOA_DYING);
        } else {
            animation_set_line(&mon->animation, MOA_HURTING);
            // TODO: play dying sound.
        }
    }
}

Baction bmon_act(Bmon *mon)
{
    // Queue as many attack actions as possible.
    if (mon->aq.len < BACTION_MAX - 1) {
        Baction action = (Baction) {
            .id = BAC_ATTACK, .target = 0, .required_time = 3000
        };
        baction_qadd(&mon->aq, action);
    }
    // Execute action if enought time has passed.
    if (mon->aq.len > 0) {
        uint32_t et = SDL_GetTicks() - mon->aq.actions[0].start_time;
        if (et >= mon->aq.actions[0].required_time) {
            return baction_qget(&mon->aq);
        }
    }
    return (Baction) { .id = BAC_NONE };
}
