#define MAX_BSLOTS 6
#define SLOT_WIDTH_UNIT 180

typedef struct {
    Bmon mons[MAX_BSLOTS];
    Rect dsts[MAX_BSLOTS];
    int remaining_slots;
    int len;
} Bslots;

Bslots bslots_create(Mon_id *mons, int len, Rect win)
{
    Bslots result = (Bslots) {.remaining_slots = MAX_BSLOTS};
    int sizes[MAX_BSLOTS];
    {// Compute used slot sizes.
        // TODO? remaning monsters queue.
        for (int i = 0; i < len; ++i) {
            int msize = bmon_size[mons[i]];
            if ((result.remaining_slots -= msize) >= 0) {
                sizes[result.len++] = msize;
            }
        };
    }
    {// Compute the slots.
        int slot_w = SLOT_WIDTH_UNIT;
        int sx = 0;
        for (int i = 0; i < result.len; ++i) {
            int sw = slot_w * sizes[i];
            result.dsts[i] = (Rect) {
                .x = sx, .w = sw,
                .y = win.y, .h = win.h
            }; sx += sw + 20;
        } sx -= 20;
        // Centralize.
        sx = (win.w - sx) / 2;
        for (int i = 0; i < result.len; ++i) {
            result.dsts[i].x += sx;
        }
    }
    {// Create the monsters
        for (int i = 0; i < result.len; ++i) {
            result.mons[i] = bmon_create(mons[i], result.dsts[i]);
        }
    }
    return result;
}

/* TODO? Maybe a monster spawns another.
void bslots_add(Bslots *slots, Mon_id mon)
{
    if ((slots->remaining_slots -= bmon_size[mon]) >= 0) {
        int len = slots->len++;
        slots->mons[len] = bmon_create(mon, slots->dsts[len]);
    }
}
*/
