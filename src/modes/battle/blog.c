#define BLOG_MAX 7
#define BLOG_CHAR_WIDTH 15
#define BLOG_LINE_HEIGHT 20

typedef struct {
    Texture *texture;
    int len;
} Log;

typedef struct {
    Log logs[BLOG_MAX];
    Rect box;
//    int color_switch;
    int last_log; // The log is a circular list.
} Blog;

Blog blog_create(Rect box)
{
    return (Blog) {.box = box};
}

void blog_add(Blog *blog, char *msg, Color color)
{   assert(blog);
    //Color color = blog->color_switch++ % 2 ? COLOR_LOG_1 : COLOR_LOG_2;
    Texture *log = video_load_texture_from_text(msg, color);
    video_destroy_texture(&blog->logs[blog->last_log].texture);
    blog->logs[blog->last_log++] = (Log) {.texture = log, .len = strlen(msg)};
    if (blog->last_log == BLOG_MAX) blog->last_log = 0;
}

void blog_draw(Blog *blog)
{   assert(blog);
    video_fill_box(blog->box, 5, COLOR_BATTLE_MENU, COLOR_BLACK);
    int log_count = 0;
    while (blog->logs[log_count].texture && ++log_count < BLOG_MAX)
        ;
    for (int i = 0, clog = blog->last_log; i < log_count; ++i, ++clog) {
        if (clog == BLOG_MAX || !blog->logs[clog].texture) clog = 0;
        Rect dst = {
            .x = blog->box.x + 6,
            .y = blog->box.y + (blog->box.h / BLOG_MAX) * i + 6,
            .w = min(BLOG_CHAR_WIDTH * blog->logs[clog].len, blog->box.w) - 6,
            .h = BLOG_LINE_HEIGHT,
        };
        video_draw_texture(blog->logs[clog].texture, NULL, &dst);
    }
}

void blog_destroy(Blog *blog)
{   assert(blog);
    for (int i = 0; i < BLOG_MAX; ++i) {
        video_destroy_texture(&blog->logs[i].texture);
    }
}
