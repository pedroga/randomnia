void new_game_mode(void)
{
    Texture *name_texture = video_load_texture_from_text("Derfel Cadarn", COLOR_WHITE);
    Rect name_dst = { .x = HU, .w = HU * 5, .y = VU, .h = VU };

    Texture *pts_texture = video_load_texture_from_text("Points", COLOR_WHITE);
    Rect pts_dst = { .x = HU, .w = HU * 5, .y = VU * 3, .h = VU };
    Rect pts_val_dst = { .x = HU * 7, .w = HU, .y = VU * 3, .h = VU };

    enum {
        NGOPT_START,
        NGOPT_STR, NGOPT_AGL, NGOPT_DEX, NGOPT_INT, NGOPT_WIL, NGOPT_REF, // Attrs.
        NGOPT_OK,
        NGOPT_END
    };

    // Attributes
    struct { Texture *texture; Rect dst; } options[] = {
        [NGOPT_STR] {
            video_load_texture_from_text("Strength", COLOR_WHITE),
            { .x = HU, .w = HU * 5, .y = VU * 4, .h = VU }
        },
        [NGOPT_AGL] {
            video_load_texture_from_text("Agility", COLOR_WHITE),
            { .x = HU, .w = HU * 5, .y = VU * 5, .h = VU }
        },
        [NGOPT_DEX] {
            video_load_texture_from_text("Dextery", COLOR_WHITE),
            { .x = HU, .w = HU * 5, .y = VU * 6, .h = VU }
        },
        [NGOPT_INT] {
            video_load_texture_from_text("Intelligence", COLOR_WHITE),
            { .x = HU, .w = HU * 5, .y = VU * 7, .h = VU }
        },
        [NGOPT_WIL] {
            video_load_texture_from_text("Willpower", COLOR_WHITE),
            { .x = HU, .w = HU * 5, .y = VU * 8, .h = VU }
        },
        [NGOPT_REF] {
            video_load_texture_from_text("Reflex", COLOR_WHITE),
            { .x = HU, .w = HU * 5, .y = VU * 9, .h = VU }
        },
        [NGOPT_OK] {
            video_load_texture_from_text("OK", COLOR_WHITE),
            { .x = HU * 8, .w = HU * 4, .y = VU * 17, .h = VU }
        },
    };

    Texture *levels[DL_TOTAL];

    {// Mode loop.
        bool loop = true;
        int cursor_idx = NGOPT_START + 1;
        int attr_levels[6];
        int free_pts = 2;
        Texture *numbers[10];
        for (int i = 0; i < sizeof(attr_levels)/sizeof(*attr_levels); ++i) attr_levels[i] = DL_FAIR;

        void redraw(void)
        {
            video_fill_box((Rect) {0, 0, VIDEO_W, VIDEO_H}, 3, COLOR_STATUS_MODE_MENU, COLOR_WHITE);
            video_draw_texture(name_texture, NULL, &name_dst);
            video_draw_texture(pts_texture, NULL, &pts_dst);
            if (free_pts < 10)
                video_draw_texture(numbers[free_pts], NULL, &pts_val_dst);
            else {
                video_draw_texture(numbers[free_pts/10], NULL, &pts_val_dst);
                Rect digit_dst = pts_val_dst; digit_dst.x += HU;
                video_draw_texture(numbers[free_pts%10], NULL, &digit_dst);
            }
            for (int i = NGOPT_START + 1; i < NGOPT_END; ++i)
                video_draw_texture(options[i].texture, NULL, &options[i].dst);
            for (int i = 0; i < 6; ++i) {
                Rect dst = {HU * 7, VU * (i + 4), HU * 4, VU};
                video_draw_texture(levels[attr_levels[i]], NULL, &dst);
            }
        }

        for (int i = 0; i < 10; ++i) {
            char c[] = "0"; c[0] += i;
            numbers[i] = video_load_texture_from_text(c, COLOR_WHITE);
        }
        for (int i = NGOPT_START + 1; i < NGOPT_END; ++i) {
            video_set_texture_color_mod(options[i].texture, COLOR_SECONDARY_BTN);
        } video_set_texture_color_mod(options[cursor_idx].texture, COLOR_PRIMARY_BTN);
        for (int i = 0; i < DL_TOTAL; ++i) {
            levels[i] = video_load_texture_from_text(dldescs[i], dlcolors[i]);
        }

        Input input = {
            [INK_BACK] = { lambda(void, () { loop = false; }) },
            [INK_UP]   = { lambda(void, () {
                video_set_texture_color_mod(options[cursor_idx].texture, COLOR_SECONDARY_BTN);
                if (--cursor_idx <= NGOPT_START) cursor_idx = NGOPT_END - 1;
                video_set_texture_color_mod(options[cursor_idx].texture, COLOR_PRIMARY_BTN);
            }) },
            [INK_DOWN]  = { lambda(void, () {
                video_set_texture_color_mod(options[cursor_idx].texture, COLOR_SECONDARY_BTN);
                if (++cursor_idx >= NGOPT_END) cursor_idx = NGOPT_START + 1;
                video_set_texture_color_mod(options[cursor_idx].texture, COLOR_PRIMARY_BTN);
            }) },
            [INK_RIGHT] = { lambda(void, () {
                if (!(NGOPT_STR <= cursor_idx && cursor_idx <= NGOPT_REF)) return;
                if (attr_levels[cursor_idx-NGOPT_STR] < DL_SUPERB && free_pts > 0) {
                    ++attr_levels[cursor_idx-NGOPT_STR]; --free_pts;
                }
            }) },
            [INK_LEFT]  = { lambda(void, () {
                if (!(NGOPT_STR <= cursor_idx && cursor_idx <= NGOPT_REF)) return;
                if (attr_levels[cursor_idx-NGOPT_STR] > DL_TERRIBLE) {
                    --attr_levels[cursor_idx-NGOPT_STR]; ++free_pts;
                }
            }) },
            [INK_A]     = { lambda(void, () {({
                if (cursor_idx == NGOPT_OK) {
                    if (free_pts) message_ok(redraw, "You must spend all pts before continuing!", false);
//                    else {
                        g_player_stats = (Stats) {
                            .name = "Cagann",
                            .toughness = TL_HEROIC,
                            .kind = STATUSK_HUMANOID,
                            .humanoid = (Humanoid_stats) {
                                .clan_name = "Nomar",
                                .attrs = {
                                    [ATTR_STR] = attr_levels[ATTR_STR],
                                    [ATTR_AGL] = attr_levels[ATTR_AGL],
                                    [ATTR_DEX] = attr_levels[ATTR_DEX],
                                    [ATTR_INT] = attr_levels[ATTR_INT],
                                    [ATTR_WIL] = attr_levels[ATTR_WIL],
                                    [ATTR_REF] = attr_levels[ATTR_REF],
                                },
                            }
                        };
                        world_generate();
                        topdown_mode(&g_player_stats);
                        world_unload();
                        loop = false;
//                    }
                }
                });
            }) }
        };

        while (loop) {
            if (g_current_music != MUSIC_THEME)
                play_music(MUSIC_THEME);
            input_handle(input);
            video_clear();
            redraw();
            video_present();
        }
    }

    {// Deinit.
        video_destroy_texture(&name_texture);
    }
}
