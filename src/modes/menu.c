#include "menu/status.c"

void menu_mode(Func_redraw *parent_redraw)
{   assert(parent_redraw);
    Rect menu_dst = {.x = HU, .w = HU * 4, .y = VU, .h = VU * 6};
    Rect sel_dst  = {.x = HU * 2, .w = HU * 2, .y = VU * 7 - VU / 2, .h = VU};
    int sel_idx = 0;
    Rect menu_name_dst = {.x = HU * 5, .w = HU * 14, .y = VU, .h = VU * 3};
    Text name_lbl = video_load_text("Name", COLOR_WHITE);
    Text name = video_load_text(g_player_stats.name, COLOR_WHITE);
    Text clan_lbl = video_load_text("Clan", COLOR_WHITE);
    Text clan = video_load_text(g_player_stats.humanoid.clan_name, COLOR_WHITE);
    Text menu_opts[] = {
        video_load_text("Status", COLOR_WHITE),
        video_load_text("Equipment", COLOR_WHITE),
        video_load_text("Inventory", COLOR_WHITE),
        video_load_text("Fighting Style", COLOR_WHITE),
        video_load_text("Party", COLOR_WHITE),
        video_load_text("Configuration", COLOR_WHITE),
    };
    Texture *menu_icons  = video_load_texture("data/textures/icons-menu.png");
    Texture *menu_cursor = video_load_texture("data/textures/arrow_target.png");
    bool loop = true;

    void redraw(void)
    {
        parent_redraw();
        video_fill_box(menu_dst, 3, COLOR_MENU, COLOR_WHITE);
        // Draw menu options.
        for (int i = 0; i < 3; ++i) {
            Rect src = {.x = i * 64, .y = 0, .w = 64, .h = 64};
            Rect dst = {.x = menu_dst.x, .w = HU * 2, .y = menu_dst.y + VU * 2 * i, .h = VU * 2};
            video_draw_texture(menu_icons, &src, &dst);
        }
        for (int i = 0; i < 3; ++i) {
            Rect src = {.x = (i + 3) * 64, .y = 0, .w = 64, .h = 64};
            Rect dst = {.x = menu_dst.x + HU * 2, .w = HU * 2, .y = menu_dst.y + VU * 2 * i, .h = VU * 2};
            video_draw_texture(menu_icons, &src, &dst);
        }
        // Draw cursor.
        Rect cursor_dst = {.x = menu_dst.x, .w = HU, .y = menu_dst.y, .h = VU};
        if (sel_idx < 3) cursor_dst.x += HU / 2;
        else cursor_dst.x += HU * 2 + HU / 2;
        cursor_dst.y += -(VU / 2) + (VU * 2 * (sel_idx % 3));
        video_draw_texture(menu_cursor, NULL, &cursor_dst);
        // Draw selected option description.
        sel_dst.w = menu_opts[sel_idx].len * DEFAULT_FONT_WIDTH;
        sel_dst.x = (menu_dst.x * 2 + menu_dst.w) / 2 - sel_dst.w / 2;
        video_fill_rect(sel_dst, COLOR_MENU_DESC);
        video_draw_texture(menu_opts[sel_idx].texture, NULL, &sel_dst);
        {// Draw name.
            video_fill_box(menu_name_dst, 3, COLOR_MENU, COLOR_WHITE);
            Rect name_lbl_dst = {
                .x = menu_name_dst.x + HU, .y = menu_name_dst.y + VU / 2,
                .w = name_lbl.len * 30, .h = VU
            };
            Rect name_dst = {
                .x = name_lbl_dst.x + HU, .y = name_lbl_dst.y + VU,
                .w = name.len * DEFAULT_FONT_WIDTH, .h = VU,
            };
            Rect clan_lbl_dst = {
                .x = menu_name_dst.x + HU * 5, .y = menu_name_dst.y + VU / 2,
                .w = name_lbl.len * 30, .h = VU
            };
            Rect clan_dst = {
                .x = clan_lbl_dst.x + HU, .y = clan_lbl_dst.y + VU,
                .w = name.len * DEFAULT_FONT_WIDTH, .h = VU,
            };
            video_draw_texture(name_lbl.texture, NULL, &name_lbl_dst);
            video_draw_texture(name.texture, NULL, &name_dst);
            video_draw_texture(clan_lbl.texture, NULL, &clan_lbl_dst);
            video_draw_texture(clan.texture, NULL, &clan_dst);
        }
//        Rect name_dst = {.x = HU, .w = HU * 6, .y = VU * 4 + 5, .h = VU};
//        video_draw_texture(name_texture, NULL, &name_dst);
    }

    Input input = {
        [INK_BACK]  = { lambda(void, () { loop = false; }) },
        [INK_UP]    = { lambda(void, () {
            if (sel_idx <= 2) {
                if (--sel_idx < 0) sel_idx = 2;
            } else if (--sel_idx < 3) sel_idx = 5;
        }) },
        [INK_DOWN]  = { lambda(void, () {
            if (sel_idx <= 2) {
                if (++sel_idx > 2) sel_idx = 0;
            } else if (++sel_idx > 5) sel_idx = 3;
        }) },
        [INK_LEFT]  = { lambda(void, () { if ((sel_idx -= 3) < 0) sel_idx += 6; }) },
        [INK_RIGHT] = { lambda(void, () { if ((sel_idx += 3) > 5) sel_idx -= 6; }) },
        [INK_A]     = { lambda(void, () {
            switch (sel_idx) {
            case 0: menu_status_mode(redraw); break;
//          case 1: menu_equipment_mode(); break;
//          case 2: menu_inventory_mode(); break;
//          case 3: menu_fs_mode(); break;
//          case 4: menu_party_mode(); break;
//          case 5: menu_configuration_mode(); break;
            default: {}
            }
        }) },
    };

    while (loop) {
        if (g_current_music != MUSIC_BATTLE) play_music(MUSIC_BATTLE);
        video_clear();
        input_handle(input);
        redraw();
        video_present();
    }
}
