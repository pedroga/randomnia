#include "battle/baction.c"
#include "battle/bmon.c"
#include "battle/bslots.c"
#include "battle/bmenu.c"
#include "battle/blog.c"

void battle_mode(Func_redraw *parent_redraw, Stats *player_stats)
{   assert(parent_redraw && player_stats && player_stats->kind == STATUSK_HUMANOID);
    // Bmenu setup.
    Bmenu bmenu = bmenu_create((Rect) {
        .x = HU * 4, .w = HU * 3,
        .y = VU * 14, .h = VU * 5 + (VU/2)
    });
    int sel_slot = 0;
    Blog blog = blog_create((Rect) {
        .x = HU * 7 - 5, .w = HU * 9,
        .y = VU * 14, .h = VU * 5 + (VU/2)
    });

    // Background setup.
    Texture *background_texture = video_load_texture("data/textures/slime_cave.png");
    Rect background_dst = {
        .x = 0, .w = VIDEO_W,
        .y = VU * 2, .h = VU * 14
    };
    int floor_level = bmenu.box.y - 64;

    // Monters setup.
    Rect slots_dst = background_dst; slots_dst.h = floor_level - background_dst.y;
    //Mon_id mons[] = {MON_SLIME_COOL, MON_SLIME_KING, MON_SLIME_CUBE};
    //Bslots mon_slots = bslots_create(mons, 3, slots_dst);
    Mon_id mons[] = {MON_SLIME_COOL};
    Bslots mon_slots = bslots_create(mons, 1, slots_dst);
    //Mon_id mons[] = {MON_FURMI, MON_FURMI, MON_FURMI, MON_FURMI, MON_FURMI, MON_FURMI};
    //Bslots mon_slots = bslots_create(mons, 6, slots_dst);
    //Mon_id mons[] = {MON_SLIME_CUBE, MON_SLIME_CUBE, MON_SLIME_CUBE};
    //Bslots mon_slots = bslots_create(mons, 3, slots_dst);
    //Mon_id mons[] = {MON_SLIME_CUBE, MON_DRAGON};
    //Bslots mon_slots = bslots_create(mons, 2, slots_dst);
    //Mon_id mons[] = {MON_DRAGON, MON_DRAGON};
    //Bslots mon_slots = bslots_create(mons, 2, slots_dst);
    //Mon_id mons[] = {MON_ENDER_DRAGON};
    //Bslots mon_slots = bslots_create(mons, 1, slots_dst);

    {// Mode loop.
        bool loop = true;
        int player_color_switch = 0, monster_color_switch = 0;
        Input input = {
            [INK_BACK]  = { lambda(void, () { loop = false; }) },
            [INK_UP]    = { lambda(void, () { bmenu_prev(&bmenu); }) },
            [INK_DOWN]  = { lambda(void, () { bmenu_next(&bmenu); }) },
            [INK_LEFT]  = { lambda(void, () { if (--sel_slot < 0) sel_slot = mon_slots.len - 1; }) },
            [INK_RIGHT] = { lambda(void, () { if (++sel_slot >= mon_slots.len) sel_slot = 0; }) },
            [INK_A]     = { lambda(void, () {({
                Baction action = (Baction) {.target = sel_slot + 1};
                assert(action.start_time == 0); assert(action.id == BAC_NONE);
                switch (bmenu.selcmd) {
                case BMENU_ATTACK: {
                    action.id = BAC_ATTACK;
                    action.required_time = 1000;
                } break;
                case BMENU_DEFEND: {
                    action.id = BAC_DEFEND;
                    action.required_time = 2000;
                } break;
                case BMENU_FLEE: {
                    action.id = BAC_FLEE;
                    action.required_time = 1000;
                } break;
                }
                baction_qadd(&bmenu.action_q, action);
                });
            }) }
        };

        void redraw(void)
        {
            parent_redraw();
            video_draw_texture(background_texture, NULL, NULL);
            video_draw_line((Point) {0, floor_level},
                            (Point) {background_dst.w, floor_level},
                            COLOR_BLACK);
            // Draw each monster.
            for (int i = 0; i < mon_slots.len; ++i)
                bmon_draw(&mon_slots.mons[i], sel_slot == i);
            // Draw the menu.
            bmenu_draw(&bmenu);
            blog_draw(&blog);
            vfx_draw();
        }

        void update(void)
        {
/*
            {// Check if player won battle.
                int remaining_monsters = mon_slots.len;
                for (int i = 0; i < mon_slots.len; ++i) {
                    if (wound_level(mon_slots.mons[i].stats) == WOUNDL_DEAD) --remaining_monsters;
                }
                if (!remaining_monsters) loop = false;// TODO: victory sub_menu.
            }
*/
            if (bmenu.action_q.len > 0) {// Player action.
                uint32_t et = SDL_GetTicks() - bmenu.action_q.actions[0].start_time;
                if (et >= bmenu.action_q.actions[0].required_time) {
                    Baction act = baction_qget(&bmenu.action_q);
                    switch (act.id) {
                    case BAC_ATTACK: {
                        char atk_log[256] = "";
                        int c = sprintf(atk_log, "Monster attacked: ");
                        Color color = player_color_switch++ % 2 ?
                            COLOR_LOG_PLAYER_1 : COLOR_LOG_PLAYER_2;
                        Attack atk = create_attack(2);
                        inflict_damage(&mon_slots.mons[act.target - 1].stats, atk, atk_log + c);
                        sprintf(atk_log + strlen(atk_log), " [monster_health = %d]",
                                wound_level(mon_slots.mons[act.target - 1].stats));
                        bmon_attacked(&mon_slots.mons[act.target - 1], atk);
                        play_sound(SOUND_PUNCH);
                        blog_add(&blog, atk_log, color);
                    } break;
                    case BAC_DEFEND: {
                        // TODO.
                    } break;
                    case BAC_FLEE: {
                        // TODO.
                    } break;
                    case BAC_NONE: break;
                    }
                }
            }
            for (int i = 0; i < mon_slots.len; ++i) {// Monsters action.
                Bmon *mon = &mon_slots.mons[i];
                if (wound_level(mon->stats) != WOUNDL_DEAD) {
                    // TODO: pass battle context to the monster.
                    Baction act = bmon_act(mon);
                    switch (act.id) {
                    case BAC_ATTACK: {
                        Color color = monster_color_switch++ % 2 ?
                            COLOR_LOG_MONSTER_1 : COLOR_LOG_MONSTER_2;
                        bmon_attack(mon, act.target);
                        vfx_play(VFX_HIT, background_dst);
                        play_sound(SOUND_PUNCH);
                        blog_add(&blog, "Player attacked", color);
                    } break;
                    case BAC_DEFEND: {
                        // TODO.
                    } break;
                    case BAC_FLEE: {
                        // TODO.
                    } break;
                    case BAC_NONE: break;
                    }
                }
            }
        }

        baction_init();
        while (loop) {
            if (g_current_music != MUSIC_BATTLE)
                play_music(MUSIC_BATTLE);
            video_clear();
            input_handle(input);
            update();
            redraw();
            video_present();
        }
    }
    {// Deinit.
        for (int i = 0; i < mon_slots.len; ++i)
            bmon_destroy(&mon_slots.mons[i]);
        bmenu_destroy(&bmenu);
        blog_destroy(&blog);
        vfx_clear();
    }
}
