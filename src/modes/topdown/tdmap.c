typedef struct {
    Texture *texture;
    Rect *src, *dst;
} Draw_info;

void tdmap_draw(Area *area, Point center)
{   assert(area);
    assert(0 <= center.x && center.x < area->w * TILE_W);
    assert(0 <= center.y && center.y < area->h * TILE_H);

    static Draw_info scheduled_objects[HORIZONTAL_TILES * VERTICAL_TILES];
    int scheduled_objects_len = 0;
    Rect camera = {
        .x = center.x - VIDEO_W / 2, .w = VIDEO_W,
        .y = center.y - VIDEO_H / 2, .h = VIDEO_H,
    };
    Point center_tile = { .x = center.x / TILE_W, .y = center.y / TILE_H };
    Texture *bg_texture = NULL;
    Rect bg_dst = {center.x - 32 - camera.x, center.y - 32 - camera.y, 64, 64};
    // Draw tiles.
    const int border = 10;
    for (int i = center_tile.y - VERTICAL_TILES / 2 - border; i < center_tile.y + VERTICAL_TILES / 2 + border; ++i) {
        if (i < 0 || i > area->h) continue;
        for (int j = center_tile.x - HORIZONTAL_TILES / 2 - border; j < center_tile.x + HORIZONTAL_TILES / 2 + border; ++j) {
            if (j < 0 || j > area->w) continue;
            Rect dst = {
                .x = j * TILE_W - camera.x, .w = TILE_W,
                .y = i * TILE_H - camera.y, .h = TILE_H,
            };
            Tile_kind tk = (0 <= i && i < area->h && 0 <= j && j < area->w) ?
                tk = area->tiles[i * area->w + j].kind : TILEK_VOID;
            switch (tk) {
            case TILEK_VOID: {
                video_fill_rect(dst, COLOR_BLACK); // TODO: draw background texture.
            } break;
            case TILEK_EXIT: {
                Exit_kind ex = area->tiles[i * area->w + j].subkind;
                Rect ex_dst = {
                    .x = dst.x + g_exits_data[ex].dbox.x * TILE_W,
                    .w = g_exits_data[ex].dbox.w * TILE_W,
                    .y = dst.y + g_exits_data[ex].dbox.y * TILE_H,
                    .h = g_exits_data[ex].dbox.h * TILE_H,
                };
                video_draw_texture(g_loaded_tile_textures[TILEK_FLOOR], NULL, &dst);
                scheduled_objects[scheduled_objects_len++] = (Draw_info) {
                    .texture = g_loaded_location_textures[ex],
                    .src = NULL, .dst = &ex_dst
                };
            } break;
            case TILEK_SHOP: {
                Shop_kind sk = area->tiles[i * area->w + j].subkind;
                video_draw_texture(g_loaded_tile_textures[TILEK_FLOOR], NULL, &dst);
                video_draw_texture(g_loaded_shop_textures[sk], NULL, &dst);
            } break;
            default: {
                video_draw_texture(g_loaded_tile_textures[tk], NULL, &dst);
            }
            }
            video_draw_rect(dst, COLOR_TRANS_BLACK);
        }
    }
    // Get the texture around the player before painting the objects.
    bg_texture = video_texture_from_texture(g_texture, bg_dst);
    for (int i = 0; i < scheduled_objects_len; ++i) // Draw scheduled objects.
        video_draw_texture(scheduled_objects[i].texture, scheduled_objects[i].src, scheduled_objects[i].dst);
    // Draw the texture around the player so it doesn't seen like its on top of the objects.
    video_draw_texture(bg_texture, NULL, &bg_dst);
    video_destroy_texture(&bg_texture); assert(!bg_texture);
}
