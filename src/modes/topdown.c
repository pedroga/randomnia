#include "topdown/tdmap.c"

#define MAX_SPEED 600

void topdown_mode(Stats *player_stats)
{   assert(player_stats);
    // Global data.
//    Texture *hero = video_load_texture("data/textures/vagrant.png");
    Animation hero = animation_create(SK_TOPDOWN, TDSPRITE_VAGRANT);
    Point player_pos = {.x = TILE_W * 1, .y = TILE_H * 3};
    Area *carea = area_load(0);

    {// Mode loop.
        bool loop = true, interact = false;
        Point player_movement = {};

        void redraw(void)
        {
            tdmap_draw(carea, player_pos);
            Rect dst = {VIDEO_W / 2 - 32, VIDEO_H / 2 - 32, 64, 64};
            animation_draw(&hero, dst);
        }

        void update(void)
        {
            static Point speed = {};
            const int acc = 15;
            static Point facing_dir;

            if (interact) {
                Point front_tile_pos = (Point) { player_pos.x / TILE_W + facing_dir.x, player_pos.y / TILE_H + facing_dir.y };
                Tile front_tile = area_get_tile_at(carea, front_tile_pos);
                switch (front_tile.kind) {
                case TILEK_SHOP: {
                    trade_mode(redraw, front_tile.data);
                } break;
                default: {
                }
                }
                interact = false; speed = player_movement = (Point) {};
            }

            speed.x += player_movement.x * acc; speed.y += player_movement.y * acc;
            {// Change player direction.
                if (speed.x && player_movement.y) {
                    speed.y = player_movement.y * abs(speed.x); speed.x = 0;
                } else if (speed.y && player_movement.x) {
                    speed.x = player_movement.x * abs(speed.y); speed.y = 0;
                }
                if (speed.y < 0) {
                    facing_dir = (Point) {.y = -1}; animation_set_line(&hero, TDANIM_NORTH);
                } else if (speed.y > 0) {
                    facing_dir = (Point) {.y =  1}; animation_set_line(&hero, TDANIM_SOUTH);
                } else if (speed.x < 0) {
                    facing_dir = (Point) {.x = -1}; animation_set_line(&hero, TDANIM_WEST);
                } else if (speed.x > 0) {
                    facing_dir = (Point) {.x =  1}; animation_set_line(&hero, TDANIM_EAST);
                }
            }
            speed.x = clamp(speed.x, -MAX_SPEED, MAX_SPEED);
            speed.y = clamp(speed.y, -MAX_SPEED, MAX_SPEED);
            {// Reduce player speed when not pressing any key.
                const int speed_reduction = 40;
                if (!player_movement.x) {
                    if (speed.x > 0) speed.x = max(0, speed.x - speed_reduction);
                    else speed.x = min(0, speed.x + speed_reduction);
                }
                if (!player_movement.y) {
                    if (speed.y > 0) speed.y = max(0, speed.y - speed_reduction);
                    else speed.y = min(0, speed.y + speed_reduction);
                }
                if (speed.x == 0 && speed.y == 0) animation_pause(&hero);
                else animation_resume(&hero);
            }

            Point prev_pos = player_pos;
            player_pos.x += speed.x / 100; player_pos.y += speed.y / 100;
            if (area_collide(&carea, prev_pos, &player_pos)) speed = (Point) {};

            /*
              {// Start a battle every 10 seconds.
              static int last_fight = 0;
              if (!last_fight) last_fight = SDL_GetTicks();
              if (ET(last_fight) > 3000) {
              battle_mode(redraw, player_stats);
              player_movement = (Point) {};
              speed = (Point) {};
              play_music(MUSIC_THEME);
              last_fight = SDL_GetTicks();
              }
              }
            */
        }

        Input input = {
            [INK_BACK]  = { lambda(void, () { loop = false; }) }, // TODO: options menu.
            [INK_UP]    = {
                .keydown_func = lambda(void, () {({ player_movement = (Point) {0, -1}; }); }),
                .keyup_func   = lambda(void, () { player_movement.y = 0; })
            },
            [INK_DOWN]  = {
                .keydown_func = lambda(void, () {({ player_movement = (Point) {0, 1}; }); }),
                .keyup_func   = lambda(void, () { player_movement.y = 0; })
            },
            [INK_LEFT]  = {
                .keydown_func = lambda(void, () {({ player_movement = (Point) {-1, 0}; }); }),
                .keyup_func   = lambda(void, () { player_movement.x = 0; })
            },
            [INK_RIGHT] = {
                .keydown_func = lambda(void, () {({ player_movement = (Point) {1, 0};  }); }),
                .keyup_func   = lambda(void, () { player_movement.x = 0; })
            },
            [INK_A]     = {
                .keydown_func = lambda(void, () { interact = true; })
            },
            [INK_Y]     = {// TEMP: for testing battles.
                .keydown_func = lambda(void, () { battle_mode(redraw, player_stats); player_movement = (Point) {}; })
            },
            [INK_START] = {
                .keydown_func = lambda(void, () { menu_mode(redraw); player_movement = (Point) {}; })
            },
        };


        while (loop) {
            input_handle(input);
            video_clear();
            update();
            redraw();
            video_present();
        }
    }
    {// Deinit.
        // TODO.
    }
}
