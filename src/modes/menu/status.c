void menu_status_mode(Func_redraw *parent_redraw)
{   assert(parent_redraw);
    Rect menu_dst = {.x = 0, .w = VIDEO_W, .y = VU * 4, .h = VU * 12};
    Texture *name_texture = video_load_texture_from_text(g_player_stats.name, COLOR_WHITE);
    struct { Texture *texture; Rect dst; } attr_textures[] = {
        {
            video_load_texture_from_text("Strength", COLOR_WHITE),
            { .x = HU, .w = HU * 5, .y = VU * 5, .h = VU }
        },
        {
            video_load_texture_from_text("Agility", COLOR_WHITE),
            { .x = HU, .w = HU * 5, .y = VU * 6, .h = VU }
        },
        {
            video_load_texture_from_text("Dextery", COLOR_WHITE),
            { .x = HU, .w = HU * 5, .y = VU * 7, .h = VU }
        },
        {
            video_load_texture_from_text("Intelligence", COLOR_WHITE),
            { .x = HU, .w = HU * 5, .y = VU * 8, .h = VU }
        },
        {
            video_load_texture_from_text("Willpower", COLOR_WHITE),
            { .x = HU, .w = HU * 5, .y = VU * 9, .h = VU }
        },
        {
            video_load_texture_from_text("Reflex", COLOR_WHITE),
            { .x = HU, .w = HU * 5, .y = VU * 10, .h = VU }
        },
    };

    {// Mode loop.
        bool loop = true;
        Input input = {
            [INK_BACK] = { lambda(void, () { loop = false; }) },
        };
        Texture *levels[DL_TOTAL];

        void redraw(void)
        {
            parent_redraw();
            video_fill_box(menu_dst, 3, COLOR_STATUS_MODE_MENU, COLOR_WHITE);
            video_fill_rect((Rect) {0, VU * 5, VIDEO_W, 3}, COLOR_WHITE);
            Rect name_dst = {.x = HU, .w = HU * 6, .y = VU * 4 + 5, .h = VU};
            video_draw_texture(name_texture, NULL, &name_dst);
            for (int i = 0; i < 6; ++i) video_draw_texture(attr_textures[i].texture, NULL, &attr_textures[i].dst);
            for (int i = 0; i < 6; ++i) {
                Rect dst = {HU * 7, VU * (i + 5), HU * 4, VU};
                video_draw_texture(levels[g_player_stats.humanoid.attrs[i]], NULL, &dst);
            }
        }

        for (int i = 0; i < DL_TOTAL; ++i) levels[i] = video_load_texture_from_text(dldescs[i], dlcolors[i]);
        while (loop) {
            if (g_current_music != MUSIC_BATTLE)
                play_music(MUSIC_BATTLE); // TODO: town_music.
            video_clear();
            input_handle(input);
            redraw();
            video_present();
        }
    }
    {// Deinit.
    }
}
