bool message_ok(Func_redraw *parent_redraw, char *msg, bool cancel_opt)
{
    // TODO: proper handling of text texture (split into several lines as needed).
    Texture *msg_texture = video_load_texture_from_text(msg, COLOR_WHITE);
    Texture *ok = video_load_texture_from_text("OK", COLOR_WHITE);
    Texture *cancel = cancel_opt ? video_load_texture_from_text("CANCEL", COLOR_WHITE) : NULL;
    bool loop = true, result = false;

    void switch_result(void)
    {
        if ((result = !result)) {
            video_set_texture_color_mod(cancel, COLOR_SECONDARY_BTN);
            video_set_texture_color_mod(ok, COLOR_PRIMARY_BTN);
        } else {
            video_set_texture_color_mod(cancel, COLOR_PRIMARY_BTN);
            video_set_texture_color_mod(ok, COLOR_SECONDARY_BTN);
        }
    }

    Input input = {
        [INK_BACK]  = { lambda(void, () { loop = result = false; }) },
        [INK_LEFT]  = { lambda(void, () { switch_result(); }) },
        [INK_RIGHT] = { lambda(void, () { switch_result(); }) },
        [INK_A]     = { lambda(void, () { loop = false; }) },
    };

    Rect menu_dst   = {.x = HU * 4, .w = HU * 12, .y = VU * 4, .h = VU * 12};
    Rect msg_dst    = {.x = HU * 4, .w = HU * 12, .y = VU * 5, .h = VU};
    Rect ok_dst     = {.x = cancel_opt ? HU * 7 : HU * 9, .w = HU * 2, .y = VU * 14, .h = VU};
    Rect cancel_dst = {.x = HU * 10, .w = HU * 4, .y = VU * 14, .h = VU};

    void redraw(void)
    {
        parent_redraw();
        video_fill_box(menu_dst, 3, COLOR_STATUS_MODE_MENU, COLOR_WHITE);
        video_draw_texture(msg_texture, NULL, &msg_dst);
        video_draw_texture(ok, NULL, &ok_dst);
        if (cancel) video_draw_texture(cancel, NULL, &cancel_dst);
    }

    if (cancel_opt) {
        video_set_texture_color_mod(cancel, COLOR_PRIMARY_BTN);
        video_set_texture_color_mod(ok, COLOR_SECONDARY_BTN);
    } else video_set_texture_color_mod(ok, COLOR_PRIMARY_BTN);

    while (loop) {
        input_handle(input);
        video_clear();
        redraw();
        video_present();
    }

    video_destroy_texture(&msg_texture);
    video_destroy_texture(&ok);
    video_destroy_texture(&cancel);

    return result;
}
