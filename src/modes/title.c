void title_mode(void)
{
    enum {
        TITLE_PLAY, TITLE_CREDITS, TITLE_EXIT,
    };
    Texture *gradient = video_render_gradient();
    Texture *title = video_load_texture_from_text("RANDOMNIA", COLOR_TITLE);
    Rect title_dst = (Rect) {
        .x = VIDEO_W / 4, .w = VIDEO_W / 2,
        .y = VIDEO_H / 2 - 50, .h = 100
    };

    Texture *play_btn[2] = {
        video_load_texture_from_text("PLAY", COLOR_SECONDARY_BTN),
        video_load_texture_from_text("PLAY", COLOR_PRIMARY_BTN)
    };
    Rect play_dst = (Rect) {
        .x = VIDEO_W / 2 - 100, .w = 200,
        .y = VIDEO_H / 2 + 50, .h = 40
    };

    Texture *credits_btn[2] = {
        video_load_texture_from_text("CREDITS", COLOR_SECONDARY_BTN),
        video_load_texture_from_text("CREDITS", COLOR_PRIMARY_BTN)
    };
    Rect credits_dst = (Rect) {
        .x = VIDEO_W / 2 - 100, .w = 200,
        .y = VIDEO_H / 2 + 100, .h = 40
    };

    Texture *exit_btn[2] = {
        video_load_texture_from_text("EXIT", COLOR_SECONDARY_BTN),
        video_load_texture_from_text("EXIT", COLOR_PRIMARY_BTN)
    };
    Rect exit_dst = (Rect) {
        .x = VIDEO_W / 2 - 100, .w = 200,
        .y = VIDEO_H / 2 + 150, .h = 40
    };

    {// Mode loop.
        bool loop = true;
        int selected_btn = TITLE_PLAY;

        Input input = {
            [INK_UP]    = { lambda(void, () { if (--selected_btn < TITLE_PLAY) selected_btn = TITLE_EXIT; }) },
            [INK_DOWN]  = { lambda(void, () { if (++selected_btn > TITLE_EXIT) selected_btn = TITLE_PLAY; }) },
            [INK_A]     = { lambda(void, () {({
                switch (selected_btn) {
                case TITLE_PLAY: {
                    new_game_mode();
                    /*
                      Stats player_stats = (Stats) {
                      .name = "Jack",
                      .toughness = TL_HEROIC,
                      .kind = STATUSK_HUMANOID,
                      };
                      world_generate();
                      puts("Lets play!");
                      topdown_mode(&player_stats);
                      world_unload();
                    */
                } break;
                case TITLE_CREDITS: {
                } break;
                case TITLE_EXIT: {
                    loop = false;
                } break;
                default: {
                    puts("Not implemented yet!");
                }}
                });
            }) }
        };

        void redraw(void)
        {
            video_draw_texture(gradient, NULL, NULL);
            video_draw_texture(title, NULL, &title_dst);
            video_draw_texture(play_btn[selected_btn == TITLE_PLAY], NULL, &play_dst);
            video_draw_texture(credits_btn[selected_btn == TITLE_CREDITS], NULL, &credits_dst);
            video_draw_texture(exit_btn[selected_btn == TITLE_EXIT], NULL, &exit_dst);
        }

        while (loop) {
            if (g_current_music != MUSIC_THEME)
                play_music(MUSIC_THEME);
            input_handle(input);
            video_clear();
            redraw();
            video_present();
        }
    }

    {// Deinit.
        video_destroy_texture(&gradient);
        video_destroy_texture(&title);
        video_destroy_texture(&play_btn[0]);
        video_destroy_texture(&play_btn[1]);
        video_destroy_texture(&credits_btn[0]);
        video_destroy_texture(&credits_btn[1]);
        video_destroy_texture(&exit_btn[0]);
        video_destroy_texture(&exit_btn[1]);
    }
}
