// generate_forest
// generate_village
// generate_town
// generate_castle
// generate_palace

// void generate_cave(Area *area) {}

void generate_mundi(World *world)
{
    // Area 0.
    world->areas[0].start = 0; // Offset 0 from world->data.
    Area *area = (Area *)world->data + world->areas[0].start;
    world->areas[0].sz = sizeof(*area) + ((area->w = 18) * (area->h = 13) * sizeof(Tile));
    for (int i = 0; i < area->w * area->h; ++i) area->tiles[i] = (Tile) {.kind = TILEK_FLOOR};
    for (int i = 1; i < area->w; ++i) area->tiles[i].kind = TILEK_WALL;
    for (int i = 1; i < area->w; ++i) area->tiles[area->w + i].kind = TILEK_WATER;
    for (int i = 2; i <= area->h; ++i) area->tiles[area->w * i - 1].kind = TILEK_WATER;
//    for (int i = 1; i <= 6; ++i) area->tiles[area->w * i + 4].kind = TILEK_WATER;
    for (int i = 1; i <= 6; ++i) area->tiles[area->w * i + 8].kind = TILEK_WATER;
    world_place_object(area, (Point) {6, 6}, (Tile) {TILEK_EXIT, EXIT_CATHEDRAL, 0x0100});

    // Area 1.
    world->areas[1].start = world->areas[0].start + world->areas[0].sz;
    area = (Area *)(world->data + world->areas[1].start);
    world->areas[1].sz = sizeof(*area) + ((area->w = 5) * (area->h = 10) * sizeof(*area->tiles));
    for (int i = 0; i < area->w * area->h; ++i) area->tiles[i] = (Tile) {.kind = TILEK_FLOOR};
    for (int i = 0; i < area->w; ++i) area->tiles[i] = (Tile) {.kind = TILEK_WATER};
    for (int i = 0; i < area->w; ++i) area->tiles[area->w * (area->h - 1) + i] = (Tile) {.kind = TILEK_WATER};
    world_place_object(area, (Point) {3, 0}, (Tile) {TILEK_EXIT, EXIT_CAVE, 0x0000});
    world_place_object(area, (Point) {2, 5}, (Tile) {TILEK_SHOP, SHOP_GENERAL, 0});
}
