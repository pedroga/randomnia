/* DICE
***********************************************************************************************************************/
#define dice_roll() dice_roll_custom(2)
int dice_roll_custom(int dice_amount)
{   assert(dice_amount > 0);
    int result = 0;
    for (int i = 0; i < dice_amount; ++i) {
        int roll = 1 + (rand() % 6);
        if (roll <= 2) --result;
        else if (roll >= 5) ++result;
    } assert(-dice_amount <= result && result <= dice_amount);
    return result;
}

/* LEVELS
***********************************************************************************************************************/
typedef enum {
    SKL_NOVICE = 1, SKL_COMPETENT, SKL_SKILLED, SKL_TALENTED, SKL_ADEPT,
    SKL_EXPERT, SKL_ACCOMPLISHED, SKL_GREAT, SKL_MASTER, SKL_GRAND_MASTER, SKL_LEGENDARY,
    SKL_TOTAL
} Skill_level;

typedef enum {
    DL_TERRIBLE, DL_POOR, DL_MEDIOCRE, DL_FAIR, DL_GOOD, DL_GREAT, DL_SUPERB,
    DL_TOTAL
} Diff_level;
const char* dldescs[] = {
    [DL_TERRIBLE] = "Terrible", [DL_POOR] = "Poor", [DL_MEDIOCRE] = "Mediocre",
    [DL_FAIR] = "Fair", [DL_GOOD] = "Good", [DL_GREAT] = "Great", [DL_SUPERB] = "Superb"
};

typedef enum {
    FATL_RESTED, FATL_OK, FATL_TIRED, FATL_EXAUSTED,
    FATL_TOTAL
} Fatigue_level;

typedef enum {
    HUNL_FULL, HUNL_OK, HUNL_HUNGRY, HUNL_STARVING,
    HUNL_TOTAL
} Hunger_level;

typedef enum {
    THIL_HYDRATED, THIL_OK, THIL_THIRSTY, THIL_DEHYDRATED,
    THIL_TOTAL
} Thirst_level;

/* HEALTH
***********************************************************************************************************************/
typedef enum {
    WOUNDK_SCRATCH, WOUNDK_LIGHT, WOUNDK_SEVERE, WOUNDK_CRIPPLING, WOUNDK_FATAL,
    WOUNDK_TOTAL
} Wound_kind;

typedef enum {
    WOUNDL_UNDAMAGED, WOUNDL_SCRATCHED, WOUNDL_HURT, WOUNDL_VERY_HURT, WOUNDL_INCAPACITATED, WOUNDL_DEAD,
    WOUNDL_TOTAL
} Wound_level;

typedef enum {
    TL_SMALL    =  1,
    TL_MEDIUM   =  2,
    TL_BIG      =  4,
    TL_HEROIC   =  8,
    TL_BOSS     = 16,
} Toughness_lvl;

#define wound_level(stats) _wound_level((stats).wounds_record, (stats).toughness)
Wound_level _wound_level(int *_wounds_record, int toughness)
{
    int wounds_record[WOUNDK_TOTAL]; // Copy so we don't modify the original.
    memcpy(wounds_record, _wounds_record, WOUNDK_TOTAL * sizeof(*wounds_record));
    for (Wound_kind i = WOUNDK_SCRATCH; i <= WOUNDK_CRIPPLING; ++i) {
        while (wounds_record[i] > max(toughness >> i, 1)) {
            --wounds_record[i];
            ++wounds_record[i + 1];
        }
    }
    if      (wounds_record[WOUNDK_FATAL])     return WOUNDL_DEAD;
    else if (wounds_record[WOUNDK_CRIPPLING]) return WOUNDL_INCAPACITATED;
    else if (wounds_record[WOUNDK_SEVERE])    return WOUNDL_VERY_HURT;
    else if (wounds_record[WOUNDK_LIGHT])     return WOUNDL_HURT;
    else if (wounds_record[WOUNDK_SCRATCH])   return WOUNDL_SCRATCHED;
    else return WOUNDL_UNDAMAGED;
}

/* MATERIALS
*****************************************************************************************/
/*
typedef enum {
    METAL_IRON, METAL_STEEL, METAL_IRONWOOD, METAL_MOONSTEEL, METAL_SUNSTEEL,
    METAL_TOTAL
} Material_metal;

typedef enum {
    CLOTH_COTTON, CLOTH_SILK,
    CLOTH_TOTAL
} Material_cloth;

typedef enum {
    LEATHER_COW, LEATHER_BEAR, LEATHER_TROLL,
    LEATHER_TOTAL
} Material_leather;
*/

typedef struct {
    enum {MAT_METAL, MAT_CLOTH, MAT_LEATHER} kind;
    uint8_t lvl;
} Material;

/* INVENTORY
*****************************************************************************************/


/* EQUIPMENT
*****************************************************************************************/
typedef enum {
    ARMOURK_LIGHT, ARMOURK_MEDIUM, ARMOURK_HEAVY, ARMOURK_PLATE
} Armour_kind;

typedef struct {
    Armour_kind kind;
    Material material;
    // TODO: armour style?
} Armour;

//#include "armour_list.c"

typedef enum {
    WEAPONK_UNARMED, WEAPONK_SMALL, WEAPONK_MEDIUM, WEAPONK_LARGE, WEAPONK_REACH, WEAPONK_SHIELD, WEAPONK_RANGED,
    WEAPONK_TOTAL
} Weapon_kind;

typedef enum {
    // Unarmed.
    WEAPONSK_FIST, WEAPONSK_CLAW, WEAPONSK_KICK,
    // Small.
    WEAPONSK_DAGGER, WEAPONSK_SHORTSWORD, WEAPONSK_HAMMER,
    // Medium.
    WEAPON_ARMING_SWORD, WEAPONSK_WAR_AXE, WEAPONSK_MACE, WEAPONSK_FALCHION,
    // Large.
    WEAPONSK_LONGSWORD, WEAPONSK_MESSER, WEAPONSK_BATTLE_AXE, WEAPONSK_WAR_HAMMER,
    // Reach.
    WEAPONSK_GREATSWORD, WEAPONSK_SPEAR, WEAPONSK_NAGINATA,
    // Shields.
    WEAPONSK_SHIELD, WEAPONSK_DUELING_SHIELD,
    // Ranged.
    WEAPONSK_BOW, WEAPONSK_CROSSBOW, WEAPONSK_SLING,
    // TODO: Magical?
    WEAPONSK_TOTAL
} Weapon_subkind;

typedef struct {
    Weapon_kind kind;
//    Material material;
} Weapon;

//#include "weapon_list.c"

typedef struct {
    Weapon mainhand, offhand;
    Armour armour;
    // TODO: accessories, ...
} Equipment;

/* STATS
*****************************************************************************************/
typedef enum {
    ATTR_STR, ATTR_AGL, ATTR_DEX, // Body.
    ATTR_INT, ATTR_WIL, ATTR_REF, // Mind.
    ATTR_TOTAL
} Attributes;

typedef struct {
    int lvl;
} Fighting_style;
#define MAX_FIGHTING_STYLE 3

typedef struct {
    char *clan_name;
    Fighting_style styles[MAX_FIGHTING_STYLE];
    int active_style;
    int attrs[ATTR_TOTAL];
    Equipment equipment;
    // TODO: flags like dual wielding.
    // TODO: skills
    // TODO: inventory
} Humanoid_stats;

typedef struct {
    int lvl;
    enum { MLSTANCE_BALANCED, MLSTANCE_AGRESSIVE, MLSTANCE_DEFENSIVE } stance;
    int atk, def, acc, eva;
    // TODO: loot
} Monster_stats;

typedef struct {
    char *name;
    int wounds_record[WOUNDK_TOTAL];
    Toughness_lvl toughness;
    enum { STATUSK_HUMANOID, STATUSK_MONSTER } kind;
    union {
        Humanoid_stats humanoid;
        Monster_stats monster;
    };
} Stats;

//#include "monster_list.c"

Stats rpgs_create_monster(Mon_id id)
{   assert(id < MON_TOTAL);
    switch (id) {
    case MON_SLIME_COOL:
        return (Stats) {
            .name = "Cool Slime",
            .toughness = TL_SMALL,
            .kind = STATUSK_MONSTER,
            .monster = (Monster_stats) {
                .lvl = 1, .atk = 1, .def = 1, .acc = 1, .eva = 1
            },
        };
    default: assert(false);
    }
//    return monster_list[id];
}

/* BATTLE
***********************************************************************************************************************/
typedef enum {
    // Slashing, piercing, bludgeoning
    DAMK_CUT, DAMK_PIERCE, DAMK_BLUNT, DAMK_MAGIC,
    DAMK_TOTAL
} Damage_kind;

/*
typedef enum {
    ELMK_NONE, ELMK_FIRE, ELMK_WATER, ELMK_AIR, ELMK_EARTH, ELMK_LIGHT, ELMK_DARK,
    ELMK_TOTAL
} Element_kind;

typedef enum {
    ATKK_PUNCH, ATKK_KICK, ATKK_SLASH, ATKK_STAB, ATKK_THRUST, ATKK_BASH, ATKK_SHOT, ATKK_MAGICAL,
    ATKK_TOTAL
} Attack_kind;

typedef struct {// TODO: add more stuff
    unsigned instant : 1;
    unsigned multiple : 1;
    unsigned intensity : 2; // 0 (quick); 1 (regular); 2 (heavy); 3 (powerful).
} Atk_mods;
*/

typedef struct {
//    Attack_kind kind;
//    Element_kind elmk;
//    uint8_t elemental_intensity;
//    uint8_t skill_level;
//    Atk_mods mods;
    // TODO: atk kind, element, skill, weapon multiplier, etc.
    Damage_kind kind;
    Wound_level atker_wl;
    uint8_t skill_lvl, mat_lvl;
} Attack;

// TODO: consider wound level.
Attack create_attack(int lvl)
{
    return (Attack) { .skill_lvl = lvl, .mat_lvl = 1 };
}

/*
typedef enum {
    DEFK_NONE, DEFK_BLOCK, DEFK_EVADE, DEFK_WARD,
    DEFK_TOTAL
} Defense_kind;

typedef struct {
    Defense_kind kind;
    int elemental_resistances[ELMK_TOTAL];
    int pts;
    // TODO: elemental resistence, etc.
} Defense;
*/

typedef struct {
    int lvl, mat_lvl;
} Defense;

void inflict_damage(Stats *target, Attack atk, char *log)
{
    int log_count = 0;
    int roll = dice_roll();
    Defense def = ({
        Defense def = {};
        switch (target->kind) {
        case STATUSK_HUMANOID: {
            Humanoid_stats *stats = &target->humanoid;
            def.lvl = stats->styles[stats->active_style].lvl;
            def.mat_lvl = stats->equipment.armour.material.lvl;
        } break;
        case STATUSK_MONSTER: {
            Monster_stats *stats = &target->monster;
            def.lvl = def.mat_lvl = stats->lvl;
        } break;
        }
        def;
    });
    // TODO: atk.skill_lvl + dex?
    int wound_mods[WOUNDL_TOTAL] = {
        [WOUNDL_HURT] = 1,
        [WOUNDL_VERY_HURT] = 2,
        [WOUNDL_INCAPACITATED] = 5,
    };
    int diff = atk.skill_lvl - def.lvl + roll - wound_mods[atk.atker_wl] + wound_mods[wound_level(*target)];
    diff = clamp(diff, DL_TERRIBLE, DL_SUPERB);
    if (diff <= DL_TERRIBLE) {// Miss.
        log_count += sprintf(log + log_count, "A terrible hit missing the target!");
    } else {// Hit.
        /*
        char desc[10] = "";
        if (diff <= DL_POOR) strncpy(desc, "poor", 10);
        else if (diff <= DL_MEDIOCRE) strncpy(desc, "mediocre", 10);
        else if (diff <= DL_FAIR) strncpy(desc, "fair", 10);
        else if (diff <= DL_GOOD) strncpy(desc, "good", 10);
        else if (diff <= DL_GREAT) strncpy(desc, "great", 10);
        else strncpy(desc, "superb", 10);
        */
        log_count += sprintf(log + log_count, "A %s hit on the target", dldescs[diff]);
        // TODO: diff += str?
        int dmg = diff + atk.mat_lvl - def.mat_lvl; // TODO: consider atk_prof vs def_prof[atk_kind].
        if (dmg >= 13) {
            log_count += sprintf(log + log_count, " causing a fatal wound!!1!");
            ++target->wounds_record[WOUNDK_FATAL];
        } else if (dmg >= 10) {
            log_count += sprintf(log + log_count, " causing a crippling wound!!");
            ++target->wounds_record[WOUNDK_CRIPPLING];
        } else if (dmg >= 7) {
            log_count += sprintf(log + log_count, " causing a severe wound!");
            ++target->wounds_record[WOUNDK_SEVERE];
        } else if (dmg >= 4) {
            log_count += sprintf(log + log_count, " causing a light wound");
            ++target->wounds_record[WOUNDK_LIGHT];
        } else if (dmg > 0) {
            log_count += sprintf(log + log_count, " causing just a scratch...");
            ++target->wounds_record[WOUNDK_SCRATCH];
        } else {
            log_count += sprintf(log + log_count, " causing no damage...");
        }
    }
}
