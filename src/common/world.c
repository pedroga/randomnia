/* Tiles
***********************************************************************************************************************/
#define HORIZONTAL_TILES 10 // How many columns of tiles are displayed on the screen.
#define VERTICAL_TILES 10 // How many lines of tiles are displayed on the screen.
#define TILE_W (VIDEO_W / HORIZONTAL_TILES)
#define TILE_H (VIDEO_H / VERTICAL_TILES)

typedef enum __attribute__((__packed__)) {
    TILEK_VOID, TILEK_FLOOR, TILEK_WALL, TILEK_WATER, TILEK_LOOT, TILEK_EXIT, TILEK_SHOP,
    TILEK_TOTAL
} Tile_kind;

typedef struct {
    Tile_kind kind;
    uint8_t subkind;
    uint16_t data;
} Tile;

/* Exits
*****************************************************************************************/
typedef enum {
    EXIT_CATHEDRAL, EXIT_CAVE,
    EXIT_TOTAL
} Exit_kind;

const struct {
    Rect dbox, cbox; // In relative tile units.
} g_exits_data[EXIT_TOTAL] = {
    [EXIT_CATHEDRAL] = {
        .dbox = {.x = -1, .w = 3, .y = -6, .h = 7},
        .cbox = {.x = -1, .w = 3, .y = -1, .h = 2},
    },
    [EXIT_CAVE] = {
        .dbox = {.x = 0, .w = 1, .y = 0, .h = 1},
        .cbox = {.x = 0, .w = 1, .y = 0, .h = 1},
    },
};

typedef struct {
    unsigned area_id : 8;
    unsigned door_id : 8;
} Exit_data;

/* Shops
*****************************************************************************************/
typedef enum {
    SHOP_GENERAL, SHOP_INN, SHOP_TAVERN,
    SHOP_TOTAL
} Shop_kind;

const struct {
    Rect dbox, cbox; // In relative tile units.
} g_shops_data[SHOP_TOTAL] = {
    [SHOP_GENERAL] = {
        .dbox = {.x = 0, .w = 1, .y = 0, .h = 1},
        .cbox = {.x = 0, .w = 1, .y = 0, .h = 1},
    }
};

typedef struct {
    // TODO: items list.
    // TODO: money available.
    // TODO: shop kind (trading goods/services offered).
} Shop;

Shop *g_shops = NULL;

typedef struct {
    unsigned shop_id: 16;
} Shop_data;

/* Worlds
***********************************************************************************************************************/
#define AREAS_MAX 42
#define WORLD_MAX_SZ (128 * MB)
#define CBOXES_MAX 128

typedef struct {
    int w, h;
    Tile tiles[]; // tiles lenght = w * h.
} Area;

typedef struct {
    // TODO: world name.
    struct { size_t start, sz; } areas[AREAS_MAX]; // Area *area = cworld->data + cworld->areas[i].start;
    size_t data_sz;
    uint8_t data[];
} World;

struct {// TODO: one per area.
    Rect cboxes[CBOXES_MAX]; // Absolute positions.
    int len;
} g_collision;

// This should be used for tiles which require collision outside their position, like exits.
void world_place_object(Area *area, Point tile_pos, Tile tile)
{   assert(g_collision.len < CBOXES_MAX);
    assert(tile_pos.x + tile_pos.y * area->w < area->w * area->h);
    area->tiles[tile_pos.x + tile_pos.y * area->w] = tile;
    switch (tile.kind) {
    case TILEK_EXIT: {
        g_collision.cboxes[g_collision.len++] = (Rect) {
            .x = tile_pos.x * TILE_W + g_exits_data[tile.subkind].cbox.x * TILE_W,
            .y = tile_pos.y * TILE_H + g_exits_data[tile.subkind].cbox.y * TILE_H,
            .w = g_exits_data[tile.subkind].cbox.w * TILE_W - 1,
            .h = g_exits_data[tile.subkind].cbox.h * TILE_H - 1,
        };
    } break;
    case TILEK_SHOP: {
        static int shops_len = 0, shops_size = 32;
        g_collision.cboxes[g_collision.len++] = (Rect) {
            .x = tile_pos.x * TILE_W + g_shops_data[tile.subkind].cbox.x * TILE_W,
            .y = tile_pos.y * TILE_H + g_shops_data[tile.subkind].cbox.y * TILE_H,
            .w = g_shops_data[tile.subkind].cbox.w * TILE_W - 1,
            .h = g_shops_data[tile.subkind].cbox.h * TILE_H - 1,
        };
        if (!g_shops || shops_len >= shops_size) g_shops = realloc(g_shops, (shops_size *= 2) * sizeof(*g_shops));
        area->tiles[tile_pos.x + tile_pos.y * area->w].data = (uint16_t) shops_len;
        g_shops[shops_len++] = (Shop) {

        };
    } break;
    default: break;
    }
}

World *cworld; // The world the player is in.

#include "area_generators.c"

// TODO: save/load worlds.
void world_generate(void)
{
    // TODO: free this.
    World *world = calloc(1, sizeof(*world) + WORLD_MAX_SZ);
    if (world) { world->data_sz = WORLD_MAX_SZ; cworld = world; }
    else { printf("Couldn't allocate memory for world creation.\n"); abort(); }
    generate_mundi(world);
}

void world_unload(void)
{
//    area_destroy(&cworld->mundi);
//    free(cworld);
//    cworld = NULL;
}

/* Areas
***********************************************************************************************************************/
Texture *g_loaded_tile_textures[TILEK_TOTAL] = {};
Texture *g_loaded_location_textures[EXIT_TOTAL] = {};
Texture *g_loaded_shop_textures[SHOP_TOTAL] = {};

Area *area_load(int area_id)
{   assert(cworld->areas[area_id].start != SIZE_MAX);
    Area *area = (Area *)(cworld->data + cworld->areas[area_id].start);

    g_loaded_tile_textures[TILEK_VOID] = NULL;
    g_loaded_tile_textures[TILEK_FLOOR] = video_load_texture("data/textures/tile-grass.png");
    g_loaded_tile_textures[TILEK_WALL] = video_load_texture("data/textures/tile_wall.png");
    g_loaded_tile_textures[TILEK_WATER] = video_load_texture("data/textures/tile_water.png");

    g_loaded_location_textures[EXIT_CATHEDRAL] = video_load_texture("data/textures/cathedral.png");
    g_loaded_location_textures[EXIT_CAVE] = video_load_texture("data/textures/entrance_cave.png");

    g_loaded_shop_textures[SHOP_GENERAL] = video_load_texture("data/textures/shop.png");

    return area;
}

void area_destroy(Area *area)
{
/*
  free(area->tiles);
  for (int i = 0; i < area->sobs_len; ++i) {
  video_destroy_texture(&area->sobs[i].texture);
  switch (area->sobs[i].kind) {
  case SOBK_DOOR: {
  area_destroy(area->sobs[i].door.to);
  } break;
  }
  }
  free(area->sobs);
  for (int i = 0; i < TILEK_TOTAL; ++i)
  video_destroy_texture(&area->textures[i]);
*/
}

bool point_inside_rect(Point p, Rect r)
{
    return r.x <= p.x && p.x <= r.x + r.w && r.y <= p.y && p.y <= r.y + r.h;
}

// TODO: get a Rect of the object instead of just a Point of its center.
bool area_collide(Area **area_ref, Point prev_pos, Point *pos)
{
    const int pos_dim = 32;
    Area *area = *area_ref;
    bool collided = false;
    Point direction     = {.x = pos->x - prev_pos.x, .y = pos->y - prev_pos.y};
    int posx            = direction.x ? pos->x + pos_dim * sign(direction.x) : pos->x;
    int posy            = direction.y > 0 ? pos->y + pos_dim : pos->y;
    Point tile_pos      = (Point) {posx >= 0 ? posx / TILE_W : -1, posy >= 0 ? posy / TILE_H : -1};
    Point prev_tile_pos = {.x = prev_pos.x / TILE_W, .y = prev_pos.y / TILE_H};
    if (prev_tile_pos.x == tile_pos.x && prev_tile_pos.y == tile_pos.y) return false; // Didn't change tile.

    Tile new_tile = (0 <= tile_pos.y && tile_pos.y < area->h && 0 <= tile_pos.x && tile_pos.x < area->w) ?
        area->tiles[tile_pos.x + tile_pos.y * area->w] : (Tile) {.kind = TILEK_VOID};
    switch (new_tile.kind) {
        void revert_to_edge(void)
        {// Adjust the pos to the edge of the previous tile acording to movement direction.
            const int push_back = 1;
            if (direction.x > 0) pos->x = prev_tile_pos.x * TILE_W + TILE_W - push_back - pos_dim;
            else if (direction.x < 0) pos->x = prev_tile_pos.x * TILE_W + push_back + pos_dim;
            else if (direction.y > 0) pos->y = prev_tile_pos.y * TILE_H + TILE_H - push_back - pos_dim;
            else if (direction.y < 0) pos->y = prev_tile_pos.y * TILE_H + push_back;
        }

    case TILEK_VOID: case TILEK_WALL: case TILEK_WATER: {
        collided = true; revert_to_edge();
    } break;
    case TILEK_EXIT: {
        // TODO: destroy loaded area.
        Exit_data loc = {};
        loc.area_id = new_tile.data >> 8;
        loc.door_id = new_tile.data & 0x00FF;
        Area *new_area = area_load(loc.area_id);
        {// Find area entrance and place player in the tile below.
            int loc_id = -1;
            bool found = false;
            for (long i = 0; i < new_area->w * new_area->h; ++i) {
                if (new_area->tiles[i].kind == TILEK_EXIT) ++loc_id;
                if (loc_id == loc.door_id) {
                    *pos = (Point) {
                        .x = (i % new_area->w) * TILE_W + TILE_W / 2,
                        .y = (i / new_area->w) * TILE_H + TILE_H + TILE_H / 4
                    };
                    collided = found = true; break;
                }
            } assert(found);
        }
        *area_ref = new_area;
    } break;
    default: {
        Point adjusted_pos = {posx, posy};
        for (int i = 0; i < g_collision.len; ++i) {
            if (point_inside_rect(adjusted_pos, g_collision.cboxes[i])) {
                collided = true; revert_to_edge(); break;
            }
        }
    }}
    return collided;
}

Tile area_get_tile_at(Area *area, Point tile_pos)
{
    return area->tiles[tile_pos.x + tile_pos.y * area->w];
}
