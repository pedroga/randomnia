const Stats monster_list[MON_TOTAL] = {
    [MON_SLIME_COOL] = (Stats) {
        .name = "Cool Slime",
        .toughness = TL_SMALL,
        .kind = STATUSK_MONSTER,
        .monster = (Monster_stats) {
            .lvl = -1,
        },
    },
};
